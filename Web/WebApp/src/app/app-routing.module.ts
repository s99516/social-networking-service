import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PostComponent } from './pages/post/post.component';
import { GroupComponent } from './pages/group/group.component';
import { UserProfileComponent } from './pages/user-profile/user-profile.component';
import { NewestPostsComponent } from './pages/newest-posts/newest-posts.component';

const routes: Routes = [
  {path: 'post', component: PostComponent},
  {path: 'group', component: GroupComponent},
  {path: 'user-profile', component: UserProfileComponent},
  {path: 'newest-posts', component: NewestPostsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
