import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TopMenuComponent } from './layout/top-menu/top-menu.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { PostComponent } from './pages/post/post.component';
import { GroupComponent } from './pages/group/group.component';
import { UserProfileComponent } from './pages/user-profile/user-profile.component';
import { NewestPostsComponent } from './pages/newest-posts/newest-posts.component';

@NgModule({
  declarations: [
    AppComponent,
    TopMenuComponent,
    PostComponent,
    GroupComponent,
    UserProfileComponent,
    NewestPostsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
