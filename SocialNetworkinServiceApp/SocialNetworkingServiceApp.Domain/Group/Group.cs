﻿using SocialNetworkingServiceApp.Common.Enums;

namespace SocialNetworkingServiceApp.Domain.Group;

public class Group
{
    public int GroupId { get; set; }

    public int AdminId { get; set; }
    public string GroupName { get; set; }
    public string GroupDescription { get; set; }
    public GroupType GroupType { get; set; }
    public string ImageHref { get; set; }
    public int MembersCount { get; set; }
    public int PostCount { get; set; }
    public bool IsBanned { get; set; }
    public Domain.User.User Admin { get; set; }

    public Group(int groupId, int adminId, string groupName, string groupDescription, GroupType groupType, string imageHref, int membersCount, int postCount, bool isBanned, Domain.User.User admin)
    {
        GroupId = groupId;
        AdminId = adminId;
        GroupName = groupName;
        GroupDescription = groupDescription;
        GroupType = groupType;
        ImageHref = imageHref;
        MembersCount = membersCount;
        PostCount = postCount;
        IsBanned = isBanned;
        Admin = admin;
    }
    public Group(int groupId, int adminId, string groupName, string groupDescription, GroupType groupType, string imageHref, int membersCount, int postCount, bool isBanned)
    {
        GroupId = groupId;
        AdminId = adminId;
        GroupName = groupName;
        GroupDescription = groupDescription;
        GroupType = groupType;
        ImageHref = imageHref;
        MembersCount = membersCount;
        PostCount = postCount;
        IsBanned = isBanned;
    }

    public Group(int adminId, string groupName, string groupDescription, GroupType groupType, string imageHref)
    {
        AdminId = adminId;
        GroupName = groupName;
        GroupDescription = groupDescription;
        GroupType = groupType;
        ImageHref = imageHref;
    }

    public void EditGroup(string groupName, string groupDescription, GroupType groupType, string imageHref)
    {
        GroupName = groupName;
        GroupDescription = groupDescription;
        GroupType = groupType;
        ImageHref = imageHref;
    }
}