﻿using System.Diagnostics.SymbolStore;

namespace SocialNetworkingServiceApp.Domain.Comment;

public class Comment
{
    public int CommentId { get; set; }
    public int? ParentCommentId { get; set; }
    public int PostId { get; set; }
    public int UserId { get; set; }
    public string UserNickName { get; set; }
    public string CommentBody { get; set; }
    public DateTime CommentDateTime { get; set; }
    public bool IsBannedComment { get; set; }
    public int SubCommentCount { get; set; }

    public Comment(int commentId, int parentCommentId, int postId, int userId,
        string userNickName, string commentBody, DateTime commentDateTime, bool isBannedComment)
    {
        CommentId = commentId;
        ParentCommentId = parentCommentId;
        PostId = postId;
        UserId = userId;
        UserNickName = userNickName;
        CommentBody = commentBody;
        CommentDateTime = commentDateTime;
        IsBannedComment = isBannedComment;
    }
    public Comment(int commentId, int postId, int userId,
        string userNickName, string commentBody, DateTime commentDateTime, bool isBannedComment)
    {
        CommentId = commentId;
        PostId = postId;
        UserId = userId;
        UserNickName = userNickName;
        CommentBody = commentBody;
        CommentDateTime = commentDateTime;
        IsBannedComment = isBannedComment;
    }
    public Comment(int commentId, int postId, int userId, string userNickName, string commentBody,
        DateTime commentDate)
    {
        CommentId = commentId;
        PostId = postId;
        UserId = userId;
        UserNickName = userNickName;
        CommentBody = commentBody;
        CommentDateTime = commentDate;
    }

    public Comment(int? createCommentParentCommentId, int createCommentPostId, int createCommentUserId, string createCommentUserNickName, string createCommentCommentBody)
    {
        ParentCommentId = createCommentParentCommentId;
        PostId = createCommentPostId;
        UserId = createCommentUserId;
        UserNickName = createCommentUserNickName;
        CommentBody = createCommentCommentBody;
        CommentDateTime = DateTime.UtcNow;
        IsBannedComment = false;
        SubCommentCount = 0;

    }

    public Comment(string commentBody)
    {
        CommentBody = commentBody;
    }

    public void EditComment(string commentBody)
    {
        CommentBody = commentBody;
    }
}