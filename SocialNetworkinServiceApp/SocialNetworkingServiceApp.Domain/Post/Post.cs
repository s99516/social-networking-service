﻿using System;
using System.Collections;

namespace SocialNetworkingServiceApp.Domain.Post
{
    public class Post
    {
        public int PostId { get; set; }
        public int UserId { get; set; } //?
        public string PostBody { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime EditionDate { get; set; }
        public int LikesCount { get; set; }
        public int CommentsCount { get; set; }
        public int ReactionsCount { get; set; }
        public bool IsBannedPost { get; set; }

        public List<Media.Media> Medias { get; set; }
        
        public Post(int postId, int userId, string postBody, DateTime creationDate,
            DateTime editionDate, int likesCount, int commentsCount, int reactionsCount, bool isBannedPost)
        {
            PostId = postId;
            UserId = userId;
            PostBody = postBody;
            CreationDate = creationDate;
            EditionDate = editionDate;
            LikesCount = likesCount;
            CommentsCount = commentsCount;
            ReactionsCount = reactionsCount;
            IsBannedPost = isBannedPost;
        }
        
        
        public Post(int postId, int userId, string postBody, DateTime creationDate,
            DateTime editionDate, int likesCount, int commentsCount, int reactionsCount, bool isBannedPost, ICollection medias)
        {
            PostId = postId;
            UserId = userId;
            PostBody = postBody;
            CreationDate = creationDate;
            EditionDate = editionDate;
            LikesCount = likesCount;
            CommentsCount = commentsCount;
            ReactionsCount = reactionsCount;
            IsBannedPost = isBannedPost;
            Medias = (List<Media.Media>)medias;
        }

        public Post(int userId, string postBody)
        {
            UserId = userId;
            PostBody = postBody;
            CreationDate = DateTime.UtcNow;
            EditionDate = DateTime.UtcNow;
            LikesCount = 0;
            CommentsCount = 0;
            ReactionsCount = 0;
            IsBannedPost = false;
        }

        public Post(string postBody)
        {
            PostBody = postBody;
        }

        public void EditPost(string postBody)
        {
            PostBody = postBody;
            EditionDate = DateTime.UtcNow;
        }
    }
}