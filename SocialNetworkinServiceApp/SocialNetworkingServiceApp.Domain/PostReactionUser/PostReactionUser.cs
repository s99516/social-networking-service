﻿namespace SocialNetworkingServiceApp.Domain.PostReactionUser;

public class PostReactionUser
{
    public int PostReactionUserId { get; set; }
    public int PostId { get; set; }
    public int ReactionId { get; set; }
    public int  UserId { get; set; }

    public PostReactionUser(int postReactionUserId, int postId, int reactionId, int userId)
    {
        PostReactionUserId = postReactionUserId;
        PostId = postId;
        ReactionId = reactionId;
        UserId = userId;
    }

    public PostReactionUser(int postId, int reactionId, int userId)
    {
        PostId = postId;
        ReactionId = reactionId;
        UserId = userId;
    }

    public void EditReaction(int reactionId)
    {
        ReactionId = reactionId;
    }
}