﻿using System;
using SocialNetworkingServiceApp.Common.Enums;
using SocialNetworkingServiceApp.Domain.DomainExceptions;

namespace SocialNetworkingServiceApp.Domain.User
{
    public class User
    {
        public User(int userId, string nickName, string firstName, string lastName, string email, string password,
            int age,
            DateTime birthDate, Gender gender, DateTime registrationDate,
            DateTime editionDate, bool isActiveUser, bool isBannedUser,
            int postCount, int followersCount, int followingCount,
            string accountDescription, string iconHref)
        {
            UserId = userId;
            NickName = nickName;
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            Password = password;
            Age = age;
            BirthDate = birthDate;
            Gender = gender;
            RegistrationDate = registrationDate;
            EditionDate = editionDate;
            IsActiveUser = isActiveUser;
            IsBannedUser = isBannedUser;
            PostCount = postCount;
            FollowersCount = followersCount;
            FollowingCount = followingCount;
            AccountDescription = accountDescription;
            IconHref = iconHref;
        }

        public User(string nickName, string firstName, string lastName, string email, string password, Gender gender,
            DateTime birthDate)
        {
            if (birthDate >= (DateTime.UtcNow.AddYears(-MinUserAge)))
            {
                throw new InvalidBirthDateException(birthDate);
            }
            BirthDate = birthDate;
            NickName = nickName;
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            Password = password;
            Gender = gender;
            RegistrationDate = DateTime.UtcNow;
            EditionDate = DateTime.UtcNow;
            IsBannedUser = false;
            IsActiveUser = true;
            PostCount = 0;
            FollowersCount = 0;
            FollowingCount = 0;
        }

        public User(string UserNickName, string UserFirstName, string UserLastName, Gender UserGender, string UserEmail,
            string UserPassword, DateTime UserBirthDate)
        {
            if (UserBirthDate >= (DateTime.UtcNow.AddYears(-MinUserAge)))
            {
                throw new InvalidBirthDateException(UserBirthDate);
            }
            
            NickName = UserNickName;
            FirstName = UserFirstName;
            LastName = UserLastName;
            Gender = UserGender;
            Email = UserEmail;
            Password = UserPassword;
            BirthDate = UserBirthDate;
            RegistrationDate = DateTime.UtcNow;
            EditionDate = DateTime.UtcNow;
            IsBannedUser = false;
            IsActiveUser = true;
            PostCount = 0;
            FollowersCount = 0;
            FollowingCount = 0;
        }

        public User(string nickName, string firstName, string lastName, Gender gender, string email, DateTime birthDate)
        {
            if (birthDate >= (DateTime.UtcNow.AddYears(-MinUserAge)))
            {
                throw new InvalidBirthDateException(birthDate);
            }
            NickName = nickName;
            FirstName = firstName;
            LastName = lastName;
            Gender = gender;
            Email = email;
            BirthDate = birthDate;
        }

        public User(string nickName, string firstName, string lastName, Gender gender)
        {
            NickName = nickName;
            FirstName = firstName;
            LastName = lastName;
            Gender = gender;
        }

        public int UserId { get; set; }
        public string NickName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public int Age { get; set; }
        public DateTime BirthDate { get; set; }
        public Gender Gender { get; set; }
        public DateTime RegistrationDate { get; set; }
        public DateTime EditionDate { get; set; }
        public bool IsActiveUser { get; set; }
        public bool IsBannedUser { get; set; }
        public int PostCount { get; set; }
        public int FollowersCount { get; set; }
        public int FollowingCount { get; set; }
        public string AccountDescription { get; set; }
        public string IconHref { get; set; }

        private int MinUserAge = 15;

        public void EditUser(string nickName, string firstName, string lastName, Gender gender, string email,
            DateTime birthDate)
        {
            if (birthDate >= (DateTime.UtcNow.AddYears(-MinUserAge)))
            {
                throw new InvalidBirthDateException(birthDate);
            }
            BirthDate = birthDate;
            NickName = nickName;
            FirstName = firstName;
            LastName = lastName;
            Gender = gender;
            Email = email;
        }
    }
}