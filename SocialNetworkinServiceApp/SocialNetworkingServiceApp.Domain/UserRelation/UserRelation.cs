﻿using System.Reflection.Metadata.Ecma335;
using System.Runtime.Serialization;
using System.Security.AccessControl;

namespace SocialNetworkingServiceApp.Domain.UserRelation;

public class UserRelation
{
    public int UserRelationId { get; set; }
    public int RelatedUserId { get; set; }
    public int RelatingUserId { get; set; }
    public DateTime FollowDate { get; set; }
    public DateTime? BlockDate { get; set; }
    public bool Follow { get; set; }
    public bool Block { get; set; }

    public UserRelation(int userRelationId, int relatedUserId, int relatingUserId,DateTime followDate, DateTime blockDate, bool follow, bool block)
    {
        UserRelationId = userRelationId;
        RelatedUserId = relatedUserId;
        RelatingUserId = relatingUserId;
        FollowDate = followDate;
        BlockDate = blockDate;
        Follow = follow;
        Block = block;
    }
    public UserRelation(int userRelationId, int relatedUserId, int relatingUserId, DateTime followDate)
    {
        UserRelationId = userRelationId;
        RelatedUserId = relatedUserId;
        RelatingUserId = relatingUserId;
        FollowDate = followDate;

    }
    public UserRelation(int relatedUserId, int relatingUserId)
    {
        RelatedUserId = relatedUserId;
        RelatingUserId = relatingUserId;
        FollowDate = DateTime.UtcNow;
        BlockDate = null;
        Follow = true;
        Block = false;
    }
}