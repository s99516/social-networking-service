﻿namespace SocialNetworkingServiceApp.Domain.Category;

public class Category
{
    public int CategoryId { get; set; }
    public string CategoryName { get; set; }
    public string? IconHref { get; set; }

    public Category(int categoryId, string categoryName, string? iconHref)
    {
        CategoryId = categoryId;
        CategoryName = categoryName;
        IconHref = iconHref;
    }
    public Category(string categoryName, string? iconHref)
    {
        CategoryName = categoryName;
        IconHref = iconHref;
    }
}