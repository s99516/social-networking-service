﻿using SocialNetworkingServiceApp.Common.Enums;

namespace SocialNetworkingServiceApp.Domain.Media;

public class Media
{
    public int MediaId { get; set; }
    public int PostId { get; set; }
    public MediaType MediaType { get; set; }
    public string MediaHref { get; set; }
    public int Order { get; set; }
    
    public Media(int mediaId, int postId, MediaType mediaType, string mediaHref, int order)
    {
        MediaId = mediaId;
        PostId = postId;
        MediaType = mediaType;
        MediaHref = mediaHref;
        Order = order;
    }
}