﻿namespace SocialNetworkingServiceApp.Domain.Reaction;

public class Reaction
{
    public int ReactionId { get; }
    public string ReactionName { get; }
    public string IconHref { get; }

    public Reaction(int reactionId, string reactionName, string iconHref)
    {
        ReactionId = reactionId;
        ReactionName = reactionName;
        IconHref = iconHref;
    }
    
    public Reaction(string reactionName, string iconHref)
    {
        ReactionName = reactionName;
        IconHref = iconHref;
    }
}