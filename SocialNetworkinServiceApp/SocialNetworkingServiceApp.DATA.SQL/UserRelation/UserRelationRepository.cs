﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SocialNetworkingServiceApp.IData.UserRelation;

namespace SocialNetworkingServiceApp.DATA.SQL.UserRelation;

public class UserRelationRepository : IUserRelationRepository
{
    private readonly SocialNetworkingServiceAppDbContext _context;
    //private IUserRelationRepository _userRelationRepositoryImplementation;

    public UserRelationRepository(SocialNetworkingServiceAppDbContext context)
    {
        _context = context;
    }
    
    public async Task<Domain.UserRelation.UserRelation> GetUserRelationById(int userRelationId)
    {
        var userRelation = await _context.UserRelation.FirstOrDefaultAsync(x => x.UserRelationId == userRelationId);
        return new Domain.UserRelation.UserRelation(
            userRelation.UserRelationId,
            userRelation.RelatedUserId,
            userRelation.RelatingUserId,
            userRelation.FollowDate
        );
    }

    public Task<IEnumerable<int>> GetRelatedUsersIdsOfUserByUserId(int userId)
    { 
       var userIds =  _context.UserRelation.Where(x => x.RelatingUserId == userId).Select(x => x.RelatedUserId).ToList();
       return Task.FromResult<IEnumerable<int>>(userIds);
    }

    public Task<IEnumerable<int>> GetRelatingUsersIdsOfUserByUserId(int userId)
    {
        var userIds =  _context.UserRelation.Where(x => x.RelatedUserId == userId).Select(x => x.RelatingUserId).ToList();
        return Task.FromResult<IEnumerable<int>>(userIds);
    }
}