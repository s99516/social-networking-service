﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SocialNetworkingServiceApp.IData.Reaction;

namespace SocialNetworkingServiceApp.DATA.SQL.Reaction;

public class ReactionRepository : IReactionRepository
{
    private readonly SocialNetworkingServiceAppDbContext _context;

    public ReactionRepository(SocialNetworkingServiceAppDbContext context)
    {
        _context = context;
    }
    public async Task<IEnumerable<Domain.Reaction.Reaction>> GetReactions()
    {
        var reactionsList = new List<Domain.Reaction.Reaction>();
        reactionsList = (from reaction in _context.Reaction
            select new Domain.Reaction.Reaction(
                reaction.ReactionName,
                reaction.IconHref
            )).ToList();
        Debug.Assert(reactionsList != null);
        return reactionsList;
    }

    public async Task DeleteReaction(int reactionId)
    {
        var reaction = await _context.Reaction.FirstOrDefaultAsync(x => x.ReactionId == reactionId);

        if (reaction != null)
        {
            _context.Reaction.Remove(reaction);
            await _context.SaveChangesAsync();
        }
    }
}