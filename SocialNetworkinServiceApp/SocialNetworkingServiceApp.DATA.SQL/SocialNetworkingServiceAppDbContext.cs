﻿using Microsoft.EntityFrameworkCore;
using SocialNetworkingServiceApp.DATA.SQL.DAO;
using SocialNetworkingServiceApp.DATA.SQL.DAOConfiguration;

namespace SocialNetworkingServiceApp.DATA.SQL
{
    public class SocialNetworkingServiceAppDbContext : DbContext
    {
        public SocialNetworkingServiceAppDbContext(DbContextOptions<SocialNetworkingServiceAppDbContext> options) :
            base(options)
        {
        }

        public virtual DbSet<DAO.Category> Category { get; set; }
        public virtual DbSet<DAO.Comment> Comment { get; set; }
        
        public virtual DbSet<Media> Media { get; set; }
        public virtual DbSet<DAO.Post> Post { get; set; }
        public virtual DbSet<PostCategory> PostCategory { get; set; }
        
        public virtual DbSet<DAO.PostReactionUser> PostReactionUser { get; set; }
        public virtual DbSet<DAO.PostTag> PostTag { get; set; }
        public virtual DbSet<DAO.Reaction> Reaction { get; set; }
        public virtual DbSet<Tag> Tag { get; set; }
        public virtual DbSet<DAO.User> User { get; set; }
        public virtual DbSet<DAO.UserRelation> UserRelation { get; set; }
        public virtual DbSet<DAO.Group> Group { get; set; }
        public virtual DbSet<DAO.UserGroup> UserGroup { get; set; }
        public virtual DbSet<DAO.GroupJoinRequest> GroupJoinRequest { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new CategoryConfiguration());
            modelBuilder.ApplyConfiguration(new CommentConfiguration());
            modelBuilder.ApplyConfiguration(new MediaConfiguration());
            modelBuilder.ApplyConfiguration(new PostConfiguration());
            modelBuilder.ApplyConfiguration(new PostCategoryConfiguration());
            modelBuilder.ApplyConfiguration(new PostReactionUserConfiguration());
            modelBuilder.ApplyConfiguration(new PostTagConfiguration());
            modelBuilder.ApplyConfiguration(new ReactionConfiguration());
            modelBuilder.ApplyConfiguration(new TagConfiguration());
            modelBuilder.ApplyConfiguration(new UserConfiguration());
            modelBuilder.ApplyConfiguration(new UserRelationConfiguration());
            modelBuilder.ApplyConfiguration(new GroupConfiguration());
            modelBuilder.ApplyConfiguration(new UserGroupConfiguration());
            modelBuilder.ApplyConfiguration(new GroupJoinRequestConfiguration());
        }
    }
}