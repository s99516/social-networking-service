﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using SocialNetworkingServiceApp.IData.Category;

namespace SocialNetworkingServiceApp.DATA.SQL.Category;

public class CategoryRepository : ICategoryRepository
{
    private readonly SocialNetworkingServiceAppDbContext _context;

    public CategoryRepository(SocialNetworkingServiceAppDbContext context)
    {
        _context = context;
    }
    
    public async Task<IEnumerable<Domain.Category.Category>> GetCategories()
    {
        var categoryList = new List<Domain.Category.Category>();
        categoryList = (from category in _context.Category
            select new Domain.Category.Category(
                category.CategoryId,
                category.CategoryName,
                category.IconHref)).ToList();
        Debug.Assert(categoryList != null);
        return categoryList;
    }
}