﻿using System.Collections.Generic;

namespace SocialNetworkingServiceApp.DATA.SQL.DAO
{
    public class Reaction
    {
        public int ReactionId { get; set; }
        public string ReactionName { get; set; }
        public string IconHref { get; set; }

        public virtual ICollection<PostReactionUser> PostReactionUsers { get; set; }
    }
}