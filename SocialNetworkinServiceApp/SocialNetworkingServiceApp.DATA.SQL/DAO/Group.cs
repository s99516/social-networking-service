﻿using System.Collections.Generic;
using SocialNetworkingServiceApp.Common.Enums;

namespace SocialNetworkingServiceApp.DATA.SQL.DAO;

public class Group
{
    public int GroupId { get; set; }
    public int AdminId { get; set; }
    public string GroupName { get; set; }
    public string GroupDescription { get; set; }
    public GroupType TypeOfGroup { get; set; }
    public string ImageHref { get; set; }
    public int MembersCount { get; set; }
    public int PostCount { get; set; }
    public bool IsBanned { get; set; }

    public virtual DAO.User Admin { get; set; }
    public virtual ICollection<UserGroup> UserGroups { get; set; }
    
    public virtual ICollection<Post> Posts { get; set; }
    
    public virtual ICollection<GroupJoinRequest> GroupJoinRequests { get; set; }

}