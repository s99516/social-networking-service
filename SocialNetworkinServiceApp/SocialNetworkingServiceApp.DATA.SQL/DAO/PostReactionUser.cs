﻿namespace SocialNetworkingServiceApp.DATA.SQL.DAO
{
    public class PostReactionUser
    {
        public int PostReactionUserId { get; set; }
        public int PostId { get; set; }
        public int ReactionId { get; set; }
        public int UserId { get; set; }

        public virtual Post Post { get; set; }
        public virtual User User { get; set; }
        public virtual Reaction Reaction { get; set; }
    }
}