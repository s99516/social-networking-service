﻿using System.Collections.Generic;

namespace SocialNetworkingServiceApp.DATA.SQL.DAO
{
    public class Tag
    {
        public int TagId { get; set; }
        public string TagName { get; set; }
        public virtual ICollection<PostTag> PostTags { get; set; }
    }
}