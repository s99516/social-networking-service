﻿using System;
using System.Collections.Generic;

namespace SocialNetworkingServiceApp.DATA.SQL.DAO
{
    public class Comment
    {
        public int CommentId { get; set; }
        public int? ParentCommentId { get; set; }
        public int PostId { get; set; }
        public int UserId { get; set; }
        public string UserNickName { get; set; }
        public string CommentBody { get; set; }
        public DateTime CommentDateTime { get; set; }
        public bool IsBannedComment { get; set; }
        public int  SubCommentCount { get; set; }

        public virtual Post Post { get; set; }
        public virtual User User { get; set; }
        public virtual Comment ParentComment { get; set; }
        public virtual ICollection<Comment> SubComments { get; set; }
    }
}