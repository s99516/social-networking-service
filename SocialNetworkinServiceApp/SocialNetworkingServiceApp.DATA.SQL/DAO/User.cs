﻿#nullable enable
using System;
using System.Collections.Generic;
using SocialNetworkingServiceApp.Common.Enums;

namespace SocialNetworkingServiceApp.DATA.SQL.DAO
{
    public class User
    {
        public int UserId { get; set; }
        public string NickName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public int Age { get; set; }
        public DateTime BirthDate { get; set; }
        public Gender Gender { get; set; }
        public DateTime RegistrationDate { get; set; }
        public DateTime EditionDate { get; set; }
        public bool IsActiveUser { get; set; }
        public bool IsBannedUser { get; set; }
        public int PostCount { get; set; }
        public int FollowersCount { get; set; }
        public int FollowingCount { get; set; }
        public string? AccountDescription { get; set; }
        public string? IconHref { get; set; }

        public virtual ICollection<UserRelation> RelatedUsers { get; set; }
        public virtual ICollection<UserRelation> RelatingUsers { get; set; }
        public virtual ICollection<Post> UsersPosts { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<PostReactionUser> PostReactionUsers { get; set; }
        public virtual ICollection<UserGroup>? UserGroups { get; set; }
        public virtual ICollection<Group> Groups { get; set; }
        public virtual ICollection<GroupJoinRequest>  GroupJoinRequests{ get; set; }
    }
}