﻿using SocialNetworkingServiceApp.Common.Enums;

namespace SocialNetworkingServiceApp.DATA.SQL.DAO;

public class GroupJoinRequest
{
    public int GroupJoinRequestId { get; set; }
    public int UserId { get; set; }
    public int GroupId { get; set; }
    public RequestStatus RequestStatus { get; set; }

    public virtual Group Group { get; set; }
    public virtual User User { get; set; }
}