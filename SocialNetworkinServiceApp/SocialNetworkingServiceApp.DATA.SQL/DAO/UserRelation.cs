﻿using System;

namespace SocialNetworkingServiceApp.DATA.SQL.DAO
{
    public class UserRelation
    {
        public int UserRelationId { get; set; }
        public int RelatedUserId { get; set; }
        public int RelatingUserId { get; set; }
        public DateTime FollowDate { get; set; }
        public DateTime? BlockDate { get; set; }
        public bool Follow { get; set; }
        public bool Block { get; set; }

        public virtual User RelatedUser { get; set; }
        public virtual User RelatingUser { get; set; }
    }
}