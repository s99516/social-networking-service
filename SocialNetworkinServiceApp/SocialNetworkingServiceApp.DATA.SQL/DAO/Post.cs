﻿using System;
using System.Collections.Generic;

namespace SocialNetworkingServiceApp.DATA.SQL.DAO
{
    public class Post
    {
        public int PostId { get; set; }
        public int UserId { get; set; }
        public int? GroupId { get; set; }
        public string PostBody { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime EditionDate { get; set; }
        public int LikesCount { get; set; }
        public int CommentsCount { get; set; }
        public int ReactionsCount { get; set; }
        public bool IsBannedPost { get; set; }

        public virtual User User { get; set; }

        public virtual Group? Group { get; set; }
        public virtual ICollection<Media> Medias { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<PostTag> PostTags { get; set; }
        public virtual ICollection<PostCategory> PostCategories { get; set; }
        public virtual ICollection<PostReactionUser> PostReactionUsers { get; set; }
    }
}