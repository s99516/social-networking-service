﻿#nullable enable
using System.Collections.Generic;

namespace SocialNetworkingServiceApp.DATA.SQL.DAO
{
    public class Category
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string? IconHref { get; set; }

        public virtual ICollection<PostCategory> PostCategories { get; set; }
    }
}