﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SocialNetworkingServiceApp.IData.Post;

namespace SocialNetworkingServiceApp.DATA.SQL.Post
{
    public class PostRepository : IPostRepository
    {
        private readonly SocialNetworkingServiceAppDbContext _context;

        public PostRepository(SocialNetworkingServiceAppDbContext context)
        {
            _context = context;
        }

        public async Task<Domain.Post.Post> GetPost(int postId)
        {
            var post = await _context.Post.FirstOrDefaultAsync(x => x.PostId == postId);
            return new Domain.Post.Post(
                post.PostId,
                post.UserId,
                post.PostBody,
                post.CreationDate,
                post.EditionDate,
                post.LikesCount,
                post.CommentsCount,
                post.ReactionsCount,
                post.IsBannedPost
            );
        }

        public async Task<int> AddPost(Domain.Post.Post post)
        {
            var PostDA0 = new DAO.Post()
            {
                UserId = post.UserId,
                PostBody = post.PostBody,
                CreationDate = post.CreationDate,
                EditionDate = post.EditionDate,
                LikesCount = post.LikesCount,
                ReactionsCount = post.ReactionsCount,
                IsBannedPost = post.IsBannedPost
            };
            var user = await _context.User.FirstOrDefaultAsync(x => x.UserId == post.UserId);
            user.PostCount += 1;
            await _context.AddAsync(PostDA0);
            await _context.SaveChangesAsync();
            return PostDA0.PostId;
        }

        public async Task EditPost(Domain.Post.Post post)
        {
            var editPost = await _context.Post.FirstOrDefaultAsync(x => x.PostId == post.PostId);
            editPost.PostBody = post.PostBody;
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Domain.Post.Post>> GetNewestPosts()
        {
            var postList = _context.Post
                .OrderByDescending(x => x.CreationDate)
                .Take(5)
                .Select(y => new Domain.Post.Post(
                    y.PostId,
                    y.UserId,
                    y.PostBody,
                    y.CreationDate,
                    y.EditionDate,
                    y.LikesCount,
                    y.CommentsCount,
                    y.ReactionsCount,
                    y.IsBannedPost));
            
            return await postList.ToListAsync();
        }

        public async Task<Domain.Post.Post> GetWholePost(int postId)
        {
            var post = await _context.Post.Include(x=>x.Medias).FirstOrDefaultAsync(x => x.PostId == postId);
            return new Domain.Post.Post(
                post.PostId, 
                post.UserId,
                post.PostBody,
                post.CreationDate,
                post.EditionDate,
                post.LikesCount,
                post.CommentsCount,
                post.ReactionsCount,
                post.IsBannedPost,
                post.Medias.Select(x =>
                    new Domain.Media.Media(
                        x.MediaId,
                        x.PostId,
                        x.MediaType,
                        x.MediaHref,
                        x.Order)
                ).ToList()
            );
        }
    }
}