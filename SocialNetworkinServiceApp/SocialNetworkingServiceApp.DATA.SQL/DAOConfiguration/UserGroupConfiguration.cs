﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SocialNetworkingServiceApp.DATA.SQL.DAO;

namespace SocialNetworkingServiceApp.DATA.SQL.DAOConfiguration;

public class UserGroupConfiguration : IEntityTypeConfiguration<UserGroup>
{
    public void Configure(EntityTypeBuilder<UserGroup> builder)
    {
        builder.HasOne(x => x.User)
            .WithMany(x => x.UserGroups)
            .OnDelete(DeleteBehavior.Cascade)
            .HasForeignKey(x => x.UserId);
        builder.HasOne(x => x.Group)
            .WithMany(x => x.UserGroups)
            .OnDelete(DeleteBehavior.Cascade)
            .HasForeignKey(x => x.GroupId);
        builder.ToTable("UserGroup");
    }
}