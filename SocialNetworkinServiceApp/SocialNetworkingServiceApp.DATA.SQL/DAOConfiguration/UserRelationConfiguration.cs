﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SocialNetworkingServiceApp.DATA.SQL.DAO;

namespace SocialNetworkingServiceApp.DATA.SQL.DAOConfiguration
{
    public class UserRelationConfiguration : IEntityTypeConfiguration<DAO.UserRelation>
    {
        public void Configure(EntityTypeBuilder<DAO.UserRelation> builder)
        {
            builder.Property(x => x.Follow).HasColumnName("IsUserFollow")
                .HasColumnType("bit");
            builder.Property(x => x.Block).HasColumnName("IsUserBlock")
                .HasColumnType("bit");
            builder.HasOne(x => x.RelatedUser)
                .WithMany(x => x.RelatedUsers)
                .OnDelete(DeleteBehavior.NoAction)
                .HasForeignKey(x => x.RelatedUserId);
            builder.HasOne(x => x.RelatingUser)
                .WithMany(x => x.RelatingUsers)
                .OnDelete(DeleteBehavior.NoAction)
                .HasForeignKey(x => x.RelatingUserId);
            builder.ToTable("UserRelation");
        }
    }
}