﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SocialNetworkingServiceApp.DATA.SQL.DAO;

namespace SocialNetworkingServiceApp.DATA.SQL.DAOConfiguration;

public class GroupConfiguration : IEntityTypeConfiguration<DAO.Group>
{
    public void Configure(EntityTypeBuilder<DAO.Group> builder)
    {
        builder.Property(x => x.GroupName).IsRequired();
        builder.Property(x => x.GroupDescription).IsRequired();
        builder.HasOne(x => x.Admin)
            .WithMany(x => x.Groups)
            .OnDelete(DeleteBehavior.Restrict)
            .HasForeignKey(x => x.AdminId);
        builder.Property(x => x.IsBanned)
            .HasColumnName("IsBannedUser")
            .HasColumnType("bit");
        builder.HasMany(x => x.Posts)
            .WithOne(x => x.Group)
            .OnDelete(DeleteBehavior.Cascade)
            .HasForeignKey(x => x.GroupId);
        builder.ToTable("Group");

    }
}