﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace SocialNetworkingServiceApp.DATA.SQL.DAOConfiguration
{
    public class PostConfiguration : IEntityTypeConfiguration<DAO.Post>
    {
        public void Configure(EntityTypeBuilder<DAO.Post> builder)
        {
            builder.Property(x => x.PostBody).IsRequired();
            builder.Property(x => x.IsBannedPost).HasColumnType("bit");
            builder.HasOne(x => x.User)
                .WithMany(x => x.UsersPosts)
                .OnDelete(DeleteBehavior.Cascade)
                .HasForeignKey(x => x.UserId);
            builder.HasOne(x => x.Group)
                .WithMany(x => x.Posts)
                .OnDelete(DeleteBehavior.Cascade)
                .HasForeignKey(x => x.GroupId);
            builder.ToTable("Post");
        }
    }
}