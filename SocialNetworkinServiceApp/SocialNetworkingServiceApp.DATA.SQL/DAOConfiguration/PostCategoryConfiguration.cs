﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SocialNetworkingServiceApp.DATA.SQL.DAO;

namespace SocialNetworkingServiceApp.DATA.SQL.DAOConfiguration
{
    public class PostCategoryConfiguration : IEntityTypeConfiguration<PostCategory>
    {
        public void Configure(EntityTypeBuilder<PostCategory> builder)
        {
            builder.HasOne(x => x.Category)
                .WithMany(x => x.PostCategories)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.CategoryId);
            builder.HasOne(x => x.Post)
                .WithMany(x => x.PostCategories)
                .OnDelete(DeleteBehavior.Cascade)
                .HasForeignKey(x => x.PostId);
            builder.ToTable("PostCategory");
        }
    }
}