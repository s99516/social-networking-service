﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace SocialNetworkingServiceApp.DATA.SQL.DAOConfiguration
{
    public class UserConfiguration : IEntityTypeConfiguration<DAO.User>
    {
        public void Configure(EntityTypeBuilder<DAO.User> builder)
        {
            builder.Property(x => x.NickName).IsRequired();
            builder.Property(x => x.FirstName).IsRequired();
            builder.Property(x => x.LastName).IsRequired();
            builder.Property(x => x.Gender).IsRequired();
            builder.Property(x => x.Age).IsRequired();
            builder.Property(x => x.IsActiveUser)
                .HasColumnName("UserActiveStatus")
                .HasColumnType("bit");
            builder.Property(x => x.IsBannedUser)
                .HasColumnName("UserBannedStatus")
                .HasColumnType("bit");
            builder.ToTable("User");
        }
    }
}