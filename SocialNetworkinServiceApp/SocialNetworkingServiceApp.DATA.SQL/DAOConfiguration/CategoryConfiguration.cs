﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SocialNetworkingServiceApp.DATA.SQL.DAO;

namespace SocialNetworkingServiceApp.DATA.SQL.DAOConfiguration
{
    public class CategoryConfiguration : IEntityTypeConfiguration<DAO.Category>
    {
        public void Configure(EntityTypeBuilder<DAO.Category> builder)
        {
            builder.Property(x => x.CategoryName).IsRequired();
            builder.ToTable("Category");
        }
    }
}