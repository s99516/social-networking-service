﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SocialNetworkingServiceApp.DATA.SQL.DAO;

namespace SocialNetworkingServiceApp.DATA.SQL.DAOConfiguration
{
    public class PostReactionUserConfiguration : IEntityTypeConfiguration<DAO.PostReactionUser>
    {
        public void Configure(EntityTypeBuilder<DAO.PostReactionUser> builder)
        {
            builder.HasOne(x => x.Post)
                .WithMany(x => x.PostReactionUsers)
                .OnDelete(DeleteBehavior.NoAction)
                .HasForeignKey(x => x.PostId);
            builder.HasOne(x => x.User)
                .WithMany(x => x.PostReactionUsers)
                .OnDelete(DeleteBehavior.NoAction)
                .HasForeignKey(x => x.UserId);
            builder.HasOne(x => x.Reaction)
                .WithMany(x => x.PostReactionUsers)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.ReactionId);
            builder.ToTable("PostReactionUser");
        }
    }
}