﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SocialNetworkingServiceApp.DATA.SQL.DAO;

namespace SocialNetworkingServiceApp.DATA.SQL.DAOConfiguration;

public class GroupJoinRequestConfiguration : IEntityTypeConfiguration<GroupJoinRequest>
{
    public void Configure(EntityTypeBuilder<GroupJoinRequest> builder)
    {
        builder.Property(x => x.RequestStatus).IsRequired();
        builder.HasOne(x => x.Group)
            .WithMany(x => x.GroupJoinRequests)
            .HasForeignKey(x => x.GroupId)
            .OnDelete(DeleteBehavior.Cascade);
        builder.HasOne(x => x.User)
            .WithMany(x => x.GroupJoinRequests)
            .HasForeignKey(x => x.UserId)
            .OnDelete(DeleteBehavior.Cascade);
    }
}