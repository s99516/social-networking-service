﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SocialNetworkingServiceApp.DATA.SQL.DAO;

namespace SocialNetworkingServiceApp.DATA.SQL.DAOConfiguration
{
    public class ReactionConfiguration : IEntityTypeConfiguration<DAO.Reaction>
    {
        public void Configure(EntityTypeBuilder<DAO.Reaction> builder)
        {
            builder.Property(x => x.ReactionName).IsRequired();
            builder.Property(x => x.IconHref).IsRequired();
            builder.ToTable("Reaction");
        }
    }
}