﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SocialNetworkingServiceApp.DATA.SQL.DAO;

namespace SocialNetworkingServiceApp.DATA.SQL.DAOConfiguration
{
    public class CommentConfiguration : IEntityTypeConfiguration<DAO.Comment>
    {
        public void Configure(EntityTypeBuilder<DAO.Comment> builder)
        {
            builder.Property(x => x.CommentBody).IsRequired();
            builder.Property(x => x.IsBannedComment).HasColumnType("bit");
            builder.HasOne(x => x.User)
                .WithMany(x => x.Comments)
                .OnDelete(DeleteBehavior.NoAction)
                .HasForeignKey(x => x.UserId);
            builder.HasOne(x => x.Post)
                .WithMany(x => x.Comments)
                .OnDelete(DeleteBehavior.NoAction)
                .HasForeignKey(x => x.PostId);
            builder.HasOne(x => x.ParentComment)
                .WithMany(x => x.SubComments)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.ParentCommentId);
            builder.ToTable("Comment");
        }
    }
}