﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SocialNetworkingServiceApp.DATA.SQL.DAO;

namespace SocialNetworkingServiceApp.DATA.SQL.DAOConfiguration
{
    public class PostTagConfiguration : IEntityTypeConfiguration<DAO.PostTag>
    {
        public void Configure(EntityTypeBuilder<DAO.PostTag> builder)
        {
            builder.HasOne(x => x.Tag)
                .WithMany(x => x.PostTags)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.TagId);
            builder.HasOne(x => x.Post)
                .WithMany(x => x.PostTags)
                .OnDelete(DeleteBehavior.Cascade)
                .HasForeignKey(x => x.PostId);
            builder.ToTable("PostTag");
        }
    }
}