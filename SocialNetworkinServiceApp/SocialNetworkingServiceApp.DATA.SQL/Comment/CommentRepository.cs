﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SocialNetworkingServiceApp.IData.Comment;
using System.Linq;

namespace SocialNetworkingServiceApp.DATA.SQL.Comment;

public class CommentRepository : ICommentRepository
{
    private readonly SocialNetworkingServiceAppDbContext _context;

    public CommentRepository(SocialNetworkingServiceAppDbContext context)
    {
        _context = context;
    }
    public async Task<Domain.Comment.Comment> GetComment(int commentId)
    {
        var comment = await _context.Comment.FirstOrDefaultAsync(x => x.CommentId == commentId);
        return new Domain.Comment.Comment(
            comment.CommentId,
            comment.PostId,
            comment.UserId,
            comment.UserNickName,
            comment.CommentBody,
            comment.CommentDateTime
        );
    }

    public async Task<IEnumerable<Domain.Comment.Comment>> GetCommentsByPostId(int postId)
    {
        var commentsList = new List<Domain.Comment.Comment>();
        commentsList = (from comment in _context.Comment where comment.PostId == postId 
            select new Domain.Comment.Comment(comment.CommentId,
                comment.PostId,
                comment.UserId,
                comment.UserNickName,
                comment.CommentBody,
                comment.CommentDateTime)).ToList();
        Debug.Assert(commentsList != null, nameof(commentsList) + " != null");
        return commentsList;
    }

    public async Task<IEnumerable<Domain.Comment.Comment>> GetCommentsByParentCommentId(int parentCommentId)
    {
        var commentsList = new List<Domain.Comment.Comment>();
        commentsList = (from comment in _context.Comment
            where comment.ParentCommentId == parentCommentId
            select new Domain.Comment.Comment(comment.CommentId,
                comment.PostId,
                comment.UserId,
                comment.UserNickName,
                comment.CommentBody,
                comment.CommentDateTime)).ToList();
        Debug.Assert(commentsList!= null);
        return commentsList;
    }

    public async Task<int> AddComment(Domain.Comment.Comment comment)
    {
        var commentDAO = new DAO.Comment()
        {
            ParentCommentId = comment.ParentCommentId,
            PostId = comment.PostId,
            UserId = comment.UserId,
            UserNickName = comment.UserNickName,
            CommentBody = comment.CommentBody,
            CommentDateTime = comment.CommentDateTime
        };
        var post = await _context.Post.FirstOrDefaultAsync(x => x.PostId == comment.PostId);
        post.CommentsCount += 1;
        if (comment.ParentCommentId != null)
        {
            var parentComment = await _context.Comment.FirstOrDefaultAsync(x => x.CommentId == comment.ParentCommentId);
            parentComment.SubCommentCount += 1;
        }
        await _context.AddAsync(commentDAO);
        await _context.SaveChangesAsync();
        return commentDAO.CommentId;
    }

    public async Task EditComment(Domain.Comment.Comment comment)
    {
        var editComment = await _context.Comment.FirstOrDefaultAsync(x=>x.CommentId == comment.CommentId);
        editComment.CommentBody = comment.CommentBody;
        await _context.SaveChangesAsync();
    }

    public async Task DeleteComment(int commentId)
    {
        var comment = await _context.Comment.FirstOrDefaultAsync(x=>x.CommentId == commentId);
        if (comment.ParentCommentId != null)
        {
            var parentComment =
                await _context.Comment.FirstOrDefaultAsync(x => x.CommentId == comment.ParentCommentId);
            parentComment.SubCommentCount -= 1;
        }
        _context.Comment.Remove(comment);
         await _context.SaveChangesAsync();
    }
}