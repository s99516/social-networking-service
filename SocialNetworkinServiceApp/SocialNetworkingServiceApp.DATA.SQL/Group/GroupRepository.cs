﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SocialNetworkingServiceApp.IData.Group;

namespace SocialNetworkingServiceApp.DATA.SQL.Group;

public class GroupRepository : IGroupRepository
{
    private readonly SocialNetworkingServiceAppDbContext _context;

    public GroupRepository(SocialNetworkingServiceAppDbContext context)
    {
        _context = context;
    }

    public async Task<Domain.Group.Group> GetGroupById(int groupId)
    {
        var groupDAO = await _context.Group.FirstOrDefaultAsync(x => x.GroupId == groupId);

        return new Domain.Group.Group(
            groupDAO.GroupId, 
            groupDAO.AdminId, 
            groupDAO.GroupName,
            groupDAO.GroupDescription,
            groupDAO.TypeOfGroup,
            groupDAO.ImageHref,
            groupDAO.MembersCount,
            groupDAO.PostCount,
            groupDAO.IsBanned);
    }

    public async Task<IEnumerable<Domain.Group.Group>> GetGroupsByName(string name)
    {
        var groupList = await _context.Group
            .Where(x => x.GroupName.Contains(name))
            .Include(x=>x.Admin)
            .Select(x => new Domain.Group.Group(
                x.GroupId,
                x.AdminId,
                x.GroupName,
                x.GroupDescription,
                x.TypeOfGroup,
                x.ImageHref,
                x.MembersCount,
                x.PostCount,
                x.IsBanned,
                new Domain.User.User(x.Admin.NickName, x.Admin.FirstName, x.Admin.LastName, x.Admin.Gender)
            ))
            .ToListAsync();
        return groupList;
    }

    public async Task<int> AddGroup(Domain.Group.Group group)
    {
        var GroupDAO = new DAO.Group()
        {
            AdminId = group.AdminId,
            GroupName = group.GroupName,
            GroupDescription = group.GroupDescription,
            TypeOfGroup = group.GroupType,
            ImageHref = group.ImageHref,
            MembersCount = 0,
            PostCount = 0,
            IsBanned = false
        };

        await _context.Group.AddAsync(GroupDAO);
        await _context.SaveChangesAsync();

        return GroupDAO.GroupId;
    }

    public async Task<bool> DeleteGroup(int groupId)
    {
        var group = await _context.Group.FirstOrDefaultAsync(x => x.GroupId == groupId);
        if (group != null)
        {
            _context.Group.Remove(group);
            await _context.SaveChangesAsync();
            return true;
        }

        return false;
    }

    public async Task EditGroup(Domain.Group.Group @group)
    {
        var editGroup = await _context.Group.FirstOrDefaultAsync(x => x.GroupId == group.GroupId);

        editGroup.GroupName = group.GroupName;
        editGroup.GroupDescription = group.GroupDescription;
        editGroup.TypeOfGroup = group.GroupType;
        editGroup.ImageHref = group.ImageHref;

        await _context.SaveChangesAsync();
    }
}