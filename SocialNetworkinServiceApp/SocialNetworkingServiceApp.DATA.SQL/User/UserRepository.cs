﻿using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Google.Protobuf.WellKnownTypes;
using SocialNetworkingServiceApp.DATA.SQL.DAO;
using SocialNetworkingServiceApp.DATA.SQL.UserRelation;
using SocialNetworkingServiceApp.IData.User;
using SocialNetworkingServiceApp.IData.UserRelation;

namespace SocialNetworkingServiceApp.DATA.SQL.User
{
    public class UserRepository : IUserRepository
    {
        private readonly SocialNetworkingServiceAppDbContext _context;


        public UserRepository(SocialNetworkingServiceAppDbContext context)
        {
            _context = context;
        }
        
        public async Task<Domain.User.User> GetUser(int userId)
        {
            var user = await _context.User.FirstOrDefaultAsync(x => x.UserId == userId);
            return new Domain.User.User(
                user.UserId,
                user.NickName,
                user.FirstName,
                user.LastName,
                user.Email,
                user.Password,
                user.Age,
                user.BirthDate,
                user.Gender,
                user.RegistrationDate,
                user.EditionDate,
                user.IsActiveUser,
                user.IsBannedUser,
                user.PostCount,
                user.FollowersCount,
                user.FollowingCount,
                user.AccountDescription,
                user.IconHref
            );
        }

        public async Task<Domain.User.User> GetUser(string nickName)
        {
            var user = await _context.User.FirstOrDefaultAsync(x => x.NickName == nickName);
            return new Domain.User.User(
                user.UserId,
                user.NickName,
                user.FirstName,
                user.LastName,
                user.Email,
                user.Password,
                user.Age,
                user.BirthDate,
                user.Gender,
                user.RegistrationDate,
                user.EditionDate,
                user.IsActiveUser,
                user.IsBannedUser,
                user.PostCount,
                user.FollowersCount,
                user.FollowingCount,
                user.AccountDescription,
                user.IconHref
            );
        }

        public async Task<int> AddUser(Domain.User.User user)
        {
            var userDAO = new DAO.User()
            {
                NickName = user.NickName,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                Password = user.Password,
                BirthDate = user.BirthDate,
                Gender = user.Gender,
                RegistrationDate = user.RegistrationDate,
                EditionDate = user.RegistrationDate,
                IsBannedUser = user.IsBannedUser,
                IsActiveUser = user.IsActiveUser,
                PostCount = user.PostCount,
                FollowersCount = user.FollowersCount,
                FollowingCount = user.FollowingCount
            };
            await _context.AddAsync(userDAO);
            await _context.SaveChangesAsync();
            return userDAO.UserId;
        }

        public async Task EditUser(Domain.User.User user)
        {
            var editUser = await _context.User.FirstOrDefaultAsync(x => x.UserId == user.UserId);
            editUser.NickName = user.NickName;
            editUser.FirstName = user.FirstName;
            editUser.LastName = user.LastName;
            editUser.Gender = user.Gender;
            editUser.Email = user.Email;
            editUser.BirthDate = user.BirthDate;
            
            await _context.SaveChangesAsync();
        }

        public async Task DeleteUser(int userId)
        {
            var user = await _context.User.FirstOrDefaultAsync(x => x.UserId == userId);
            if (user != null) 
            {
                _context.User.Remove(user);
                await _context.SaveChangesAsync();
            }
        }

        public async Task AddUserToGroup(int userId, int groupId)
        {
            var userGroupDAO = new UserGroup()
            {
                UserId = userId,
                GroupId = groupId
            };
            await _context.UserGroup.AddAsync(userGroupDAO);
            await _context.SaveChangesAsync();
        }
    }
}