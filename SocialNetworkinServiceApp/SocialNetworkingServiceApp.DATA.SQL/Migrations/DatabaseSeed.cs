﻿using System;
using System.Collections.Generic;
using System.Linq;
using SocialNetworkingServiceApp.Common.Enums;
using SocialNetworkingServiceApp.Common.Extensions;
using SocialNetworkingServiceApp.DATA.SQL.DAO;

namespace SocialNetworkingServiceApp.DATA.SQL.Migrations
{
    public class DatabaseSeed
    {
        private readonly SocialNetworkingServiceAppDbContext _context;

        public DatabaseSeed(SocialNetworkingServiceAppDbContext context)
        {
            _context = context;
        }
        
        Func<DateTime> RandomDayFunc()
        {
            DateTime start = new DateTime(2020, 1, 1); 
            Random gen = new Random(); 
            int range = ((TimeSpan)(DateTime.Today - start)).Days; 
            return () => start.AddDays(gen.Next(range));
        }
        

        public void Seed()
        {
            //filling tables without foreign keys first
            var userList = BuildUserList();
            _context.User.AddRange(userList);
            _context.SaveChanges();

            var tagList = BuilTagList();
            _context.Tag.AddRange(tagList);
            _context.SaveChanges();

            var categoryList = BuildCategoryList();
            _context.Category.AddRange(categoryList);
            _context.SaveChanges();

            var reactionList = BuildReactionList();
            _context.Reaction.AddRange(reactionList);
            _context.SaveChanges();
            
            var groupList = BuildGroupList(userList);
            _context.Group.AddRange(groupList);
            _context.SaveChangesAsync();

            //filling tables with foreign keys
            var postList = BuildPostList(userList, groupList);
            _context.Post.AddRange(postList);
            _context.SaveChanges();

            var postTagList = BuildPostTagList(postList, tagList);
            _context.PostTag.AddRange(postTagList);
            _context.SaveChanges();

            var postCategoryList = BuildPostCategoryList(postList, categoryList);
            _context.PostCategory.AddRange(postCategoryList);
            _context.SaveChanges();

            var mediaList = BuildMediaList(postList);
            _context.AddRange(mediaList);
            _context.SaveChanges();

            var commentList = BuildCommentList(userList, postList);
            _context.AddRange(commentList);
            _context.SaveChanges();

            var postReactionUserList = BuildPostReactionUserList(userList, postList, reactionList);
            _context.AddRange(postReactionUserList);
            _context.SaveChanges();

            var userRelationList = BuildUserRelationList(userList);
            _context.UserRelation.AddRange(userRelationList);
            _context.SaveChanges();

            var userGroupList = BuildUserGroupList(userList, groupList);
            _context.UserGroup.AddRange(userGroupList);
            _context.SaveChanges();
        }

        private IEnumerable<DAO.User> BuildUserList()
        {
            var userList = new List<DAO.User>();
            var rnd = new Random();
            for (var i = 1; i < 21; i++)
            {
                var user = new DAO.User
                {
                    //UserId = i,
                    NickName = "nick" + i,
                    FirstName = "userFirstName" + i,
                    LastName = "userLastName" + i,
                    Email = i + "@mail.com",
                    Password = "secret" + i,
                    Age = rnd.Next(15, 100),
                    BirthDate = DateTime.UtcNow,
                    Gender = ParityExtension.IsEven(i) ? Gender.Male : Gender.Female,
                    RegistrationDate = DateTime.UtcNow,
                    EditionDate = DateTime.UtcNow,
                    IsActiveUser = true,
                    IsBannedUser = true,
                    PostCount = 0,
                    FollowersCount = 0,
                    FollowingCount = 0,
                    AccountDescription = "this is " + i + "account description",
                    IconHref = "#href#" + i
                };
                userList.Add(user);
            }

            return userList;
        }

        private IEnumerable<Tag> BuilTagList()
        {
            var TagNames = new List<string>
                {"livestyle", "me", "ootd", "gym", "car", "motorbike", "food", "party", "trip", "travel"};
            var tagList = new List<Tag>();
            var counter = 0;
            foreach (var tagName in TagNames)
            {
                counter++;
                var tag = new Tag
                {
                    //TagId = counter,
                    TagName = tagName
                };
                tagList.Add(tag);
            }

            return tagList;
        }

        private IEnumerable<DAO.Category> BuildCategoryList()
        {
            var categoryNames = new List<string>
                {"announcement", "event", "question", "sale offer", "purchase offer", "regular post"};
            var categoryList = new List<DAO.Category>();
            var counter = 0;
            foreach (var categoryName in categoryNames)
            {
                counter++;
                var category = new DAO.Category
                {
                    //CategoryId = counter,
                    CategoryName = categoryName,
                    IconHref = "#href#"
                };
                categoryList.Add(category);
            }

            return categoryList;
        }

        private IEnumerable<DAO.Reaction> BuildReactionList()
        {
            var reactionNames = new List<string>
            {
                "happy face", "sad face", "angry face", "neutral face", "open mouth face", "thumb up", "thumb down",
                "hearth", "broken hearth"
            };
            var reactionList = new List<DAO.Reaction>();
            var counter = 0;
            foreach (var reactionName in reactionNames)
            {
                counter++;
                var reaction = new DAO.Reaction
                {
                    //ReactionId = counter,
                    ReactionName = reactionName,
                    IconHref = "#href#"
                };
                reactionList.Add(reaction);
            }

            return reactionList;
        }

        private IEnumerable<DAO.Post> BuildPostList
            (IEnumerable<DAO.User> userList, IEnumerable<DAO.Group> groupsList)
        {
            var date = RandomDayFunc();
            var postList = new List<DAO.Post>();
            var random = new Random();
            for (var i = 0; i < userList.Count(); i++)
            {
                var creationDate = date.Invoke();
                var randomGroupId = RandomUtils<DAO.Group>.ChoiceFromList(groupsList.ToList()).GroupId;
                var randomUserId = random.Next(0, userList.Count() - 1);
                var post = new DAO.Post
                {
                    //PostId = i + 1,
                    UserId = randomUserId + 1,
                    GroupId = null,
                    PostBody = (i + 1) + " post body by " + userList.ToList()[randomUserId].FirstName,
                    CreationDate = creationDate,
                    EditionDate = creationDate,
                    LikesCount = 0,
                    CommentsCount = 0,
                    ReactionsCount = 0,
                    IsBannedPost = false
                };
                postList.Add(post);
                userList.ToList()[randomUserId].PostCount += 1;
            }

            return postList;
        }

        private IEnumerable<DAO.PostTag> BuildPostTagList
            (IEnumerable<DAO.Post> postList, IEnumerable<Tag> tagList)
        {
            var postTagList = new List<DAO.PostTag>();
            var tagCounter = tagList.Count();
            var postTagIdCounter = 1;
            var random = new Random();
            foreach (var post in postList)
            {
                var postTagNumber = random.Next(0, 3);
                for (var i = 0; i < postTagNumber; i++)
                {
                    var postTag = new DAO.PostTag
                    {
                        //PostTagId = postTagIdCounter,
                        PostId = post.PostId,
                        TagId = random.Next(1, tagCounter)
                    };
                    postTagIdCounter += 1;
                    postTagList.Add(postTag);
                }
            }

            return postTagList;
        }

        private IEnumerable<PostCategory> BuildPostCategoryList
            (IEnumerable<DAO.Post> postList, IEnumerable<DAO.Category> categoryList)
        {
            var postCategoryList = new List<PostCategory>();
            var counter = 0;
            var random = new Random();
            foreach (var post in postList)
            {
                counter++;
                var postCategory = new PostCategory
                {
                    //PostCategoryId = counter,
                    PostId = post.PostId,
                    CategoryId = random.Next(1, categoryList.Count() + 1)
                };
                postCategoryList.Add(postCategory);
            }

            return postCategoryList;
        }

        private IEnumerable<Media> BuildMediaList
            (IEnumerable<DAO.Post> postList)
        {
            var mediaList = new List<Media>();
            var counter = 0;
            foreach (var post in postList)
            {
                counter++;
                var media = new Media
                {
                    //MediaId = counter,
                    PostId = post.PostId,
                    MediaType = RandomUtils<MediaType>.Choice(MediaType.Audio, MediaType.Image, MediaType.Video),
                    MediaHref = "#href"
                };
                mediaList.Add(media);
            }

            return mediaList;
        }

        private IEnumerable<DAO.Comment> BuildCommentList
            (IEnumerable<DAO.User> userList, IEnumerable<DAO.Post> postList)
        {
            var commentList = new List<DAO.Comment>();
            var counter = 0;
            var random = new Random();
            var postsCount = postList.ToList().Count;
            foreach (var user in userList)
            {
                counter++;

                var postId = random.Next(postsCount);
                commentList.Add(new DAO.Comment
                {
                    PostId = postList.ToList()[postId].PostId,
                    UserId = user.UserId,
                    UserNickName = user.NickName,
                    CommentBody = "comment" + counter,
                    CommentDateTime = DateTime.Now,
                    IsBannedComment = false,
                    SubCommentCount = 0
                });
                postList.ToList()[postId].CommentsCount += 1;
            }

            return commentList;
        }

        private IEnumerable<DAO.PostReactionUser> BuildPostReactionUserList
            (IEnumerable<DAO.User> userList, IEnumerable<DAO.Post> postList, IEnumerable<DAO.Reaction> reactionList)
        {
            var postReactionUserList = new List<DAO.PostReactionUser>();
            var counter = 0;
            foreach (var user in userList)
            {
                counter++;
                var randomPost = RandomUtils<DAO.Post>.ChoiceFromList(postList.ToList());
                var randomReaction = RandomUtils<DAO.Reaction>.ChoiceFromList(reactionList.ToList());
                var postReactionUser = new DAO.PostReactionUser
                {
                    //PostReactionUserId = counter,
                    PostId = randomPost.PostId,
                    ReactionId = randomReaction.ReactionId,
                    UserId = user.UserId
                };
                postList.ToList()[randomPost.PostId - 1].ReactionsCount += 1;
                postReactionUserList.Add(postReactionUser);
            }

            return postReactionUserList;
        }

        private IEnumerable<DAO.UserRelation> BuildUserRelationList
            (IEnumerable<DAO.User> userList)
        {
            var userRelationList = new List<DAO.UserRelation>();
            var counter = 0;
            foreach (var user in userList)
                for (var i = 0; i < 3; i++)
                {
                    var randomUser = RandomUtils<DAO.User>.ChoiceFromList(userList.ToList());
                    counter++;
                    var userRelation = new DAO.UserRelation
                    {
                        //UserRelationId = counter,
                        RelatedUserId = user.UserId,
                        RelatingUserId = randomUser.UserId,
                        BlockDate = null,
                        FollowDate = DateTime.UtcNow,
                        Follow = true,
                        Block = false
                    };
                    userList.ToList()[user.UserId - 1].FollowersCount += 1;
                    userList.ToList()[randomUser.UserId - 1].FollowingCount += 1;
                    userRelationList.Add(userRelation);
                }

            return userRelationList;
        }
        private IEnumerable<DAO.Group> BuildGroupList(IEnumerable<DAO.User> usersList)
        {
            var groupList = new List<DAO.Group>();
            var enumsValues = Enum.GetValues(typeof(GroupType));
            Random random = new Random();
            for (int i = 0; i < 20; i++)
            {
                var group = new DAO.Group
                {
                    //GroupId = i + 1,
                    AdminId = RandomUtils<DAO.User>.ChoiceFromList(usersList.ToList()).UserId,
                    GroupName = "GroupName" + (i + 1),
                    GroupDescription = "GroupDescription" + (i + 1),
                    TypeOfGroup = (GroupType)random.Next(enumsValues.Length),
                    ImageHref = "#href" + (i + 1),
                    MembersCount = 0,
                    PostCount = 0,
                    IsBanned = false
                };
                groupList.Add(group);
            }

            return groupList;
        }

        private IEnumerable<UserGroup> BuildUserGroupList(IEnumerable<DAO.User> users, IEnumerable<DAO.Group> groups)
        {
            var userGroupList = new List<UserGroup>();
            var usersCount = users.Count();
            var groupsCount = groups.Count();
            Random random = new Random();
            for (int i = 0; i < 20; i++)
            {
                var userGroup = new UserGroup()
                {
                    //UserGroupId = i + 1,
                    UserId = random.Next(1, usersCount + 1),
                    GroupId = random.Next(1, groupsCount + 1),
                };
                userGroupList.Add(userGroup);
            }

            return userGroupList;
        }
    }
}