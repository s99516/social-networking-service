﻿namespace SocialNetworkingServiceApp.API.ViewModels;

public class CategoryViewModel
{
    public int CategoryId { get; set; }
    public string CategoryName { get; set; }
    public string? IconHref { get; set; }
}