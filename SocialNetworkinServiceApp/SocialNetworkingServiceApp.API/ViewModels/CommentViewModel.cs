﻿using System;

namespace SocialNetworkingServiceApp.API.ViewModels;

public class CommentViewModel
{
    public int PostId { get; set; }
    public int UserId { get; set; }
    public string UserNickName { get; set; }
    public string CommentBody { get; set; }
    public DateTime CommentDateTime { get; set; }
}