﻿using SocialNetworkingServiceApp.Common.Enums;

namespace SocialNetworkingServiceApp.API.ViewModels;

public class GroupAdminViewModel
{
    public string NickName { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public Gender Gender { get; set; }
}