﻿namespace SocialNetworkingServiceApp.API.ViewModels;

public class ReactionViewModel
{
    //public int ReactionId { get; set; }
    public string ReactionName { get; set; }
    public string IconHref { get; set; }
}