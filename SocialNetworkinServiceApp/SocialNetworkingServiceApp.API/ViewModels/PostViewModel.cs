﻿using System;

namespace SocialNetworkingServiceApp.API.ViewModels
{
    public class PostViewModel
    {
        public int PostId { get; set; }
        public int UserId { get; set; } //?
        public string PostBody { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime EditionDate { get; set; }
        public int LikesCount { get; set; }
        public int CommentsCount { get; set; }
        public int ReactionsCount { get; set; }
        //public bool IsBannedPost { get; set; }
    }
}