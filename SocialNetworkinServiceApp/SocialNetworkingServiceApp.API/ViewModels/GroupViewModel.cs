﻿using SocialNetworkingServiceApp.Common.Enums;

namespace SocialNetworkingServiceApp.API.ViewModels;

public class GroupViewModel
{
    public int GroupId { get; set; }
    public int AdminId { get; set; }
    public string GroupName { get; set; }
    public string GroupDescription { get; set; }
    public GroupType GroupType { get; set; }
    public string ImageHref { get; set; }
    public int MembersCount { get; set; }
    public int PostCount { get; set; }
    public GroupAdminViewModel Admin { get; set; }
}