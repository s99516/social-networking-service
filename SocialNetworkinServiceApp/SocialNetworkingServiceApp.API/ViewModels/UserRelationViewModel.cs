﻿using System;

namespace SocialNetworkingServiceApp.API.ViewModels;

public class UserRelationViewModel
{
    public int UserRelationId { get; set; }
    public int RelatedUserId { get; set; }
    public int RelatingUserId { get; set; }
    public DateTime FollowDate { get; set; }
}