using FluentValidation;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using SocialNetworkingServiceApp.API.BindingModels;
using SocialNetworkingServiceApp.API.Middlewares;
using SocialNetworkingServiceApp.API.Validation;
using SocialNetworkingServiceApp.DATA.SQL;
using SocialNetworkingServiceApp.DATA.SQL.Category;
using SocialNetworkingServiceApp.DATA.SQL.Comment;
using SocialNetworkingServiceApp.DATA.SQL.Group;
using SocialNetworkingServiceApp.DATA.SQL.Migrations;
using SocialNetworkingServiceApp.DATA.SQL.Post;
using SocialNetworkingServiceApp.DATA.SQL.Reaction;
using SocialNetworkingServiceApp.DATA.SQL.User;
using SocialNetworkingServiceApp.DATA.SQL.UserRelation;
using SocialNetworkingServiceApp.IData.Category;
using SocialNetworkingServiceApp.IData.Comment;
using SocialNetworkingServiceApp.IData.Group;
using SocialNetworkingServiceApp.IData.Post;
using SocialNetworkingServiceApp.IData.Reaction;
using SocialNetworkingServiceApp.IData.User;
using SocialNetworkingServiceApp.IData.UserRelation;
using SocialNetworkingServiceApp.IServices.Category;
using SocialNetworkingServiceApp.IServices.Comment;
using SocialNetworkingServiceApp.IServices.Group;
using SocialNetworkingServiceApp.IServices.Reaction;
using SocialNetworkingServiceApp.IServices.User;
using SocialNetworkingServiceApp.IServices.UserRelation;
using SocialNetworkingSeviceApp.Service.Category;
using SocialNetworkingSeviceApp.Service.Comment;
using SocialNetworkingSeviceApp.Service.Group;
using SocialNetworkingSeviceApp.Service.Reaction;
using SocialNetworkingSeviceApp.Service.User;
using SocialNetworkingSeviceApp.Service.UserRelation;

namespace SocialNetworkingServiceApp.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        
        

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<SocialNetworkingServiceAppDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("SocialNetworkingServiceAppDbContext")));
            services.AddTransient<DatabaseSeed>();
            services.AddControllers()
                .AddNewtonsoftJson(options =>
                {
                    options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                    options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                })
                .AddFluentValidation();
            services.AddTransient<IValidator<CreateUser>, CreateUserValidation>();
            services.AddTransient<IValidator<EditUser>, EditUserValidation>();
            services.AddTransient<IValidator<CreatePost>, CreatePostValidation>();
            services.AddTransient<IValidator<EditPost>, EditPostValidation>();
            services.AddTransient<IValidator<CreateComment>, CreateCommentValidation>();
            services.AddTransient<IValidator<EditComment>, EditCommentValidation>();
            
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IPostService, PostService>();
            services.AddScoped<IPostRepository, PostRepository>();
            services.AddScoped<ICommentService, CommentService>();
            services.AddScoped<ICommentRepository, CommentRepository>();
            services.AddScoped<IUserRelationService, UserRelationService>();
            services.AddScoped<IUserRelationRepository, UserRelationRepository>();
            services.AddScoped<IReactionService, ReactionService>();
            services.AddScoped<IReactionRepository, ReactionRepository>();
            services.AddScoped<ICategoryService, CategoryService>();
            services.AddScoped<ICategoryRepository, CategoryRepository>();
            services.AddScoped<IGroupService, GroupService>();
            services.AddScoped<IGroupRepository, GroupRepository>();
            
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                        .AllowAnyHeader()
                        .AllowAnyMethod());
            });

            services.AddApiVersioning(o =>
            {
                o.ReportApiVersions = true;
                o.UseApiBehavior = false;
            });

            //services.AddMvc(options => options.EnableEndpointRouting = false)
            //.SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment()) app.UseDeveloperExceptionPage();

            using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetRequiredService<SocialNetworkingServiceAppDbContext>();
                var databaseSeed = serviceScope.ServiceProvider.GetRequiredService<DatabaseSeed>();
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();
                databaseSeed.Seed();
            }


            
            //app.UseMiddleware<ErrorHandlerMiddleware>();
            app.UseRouting();
            app.UseCors("CorsPolicy");

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
            // app.UseHttpsRedirection();
            // app.UseStaticFiles();
            // app.UseCookiePolicy();
            // app.UseMvc();
        }
    }
}