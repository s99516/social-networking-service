﻿using SocialNetworkingServiceApp.API.ViewModels;
using SocialNetworkingServiceApp.Domain.User;

namespace SocialNetworkingServiceApp.API.Mappers
{
    public class UserToUserViewModelMapper
    {
        public static UserViewModel UserToUserViewModel(User user)
        {
            var userViewModel = new UserViewModel
            {
                UserId = user.UserId,
                NickName = user.NickName,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                Age = user.Age,
                BirthDate = user.BirthDate,
                Gender = user.Gender,
                RegistrationDate = user.RegistrationDate,
                EditionDate = user.EditionDate,
                PostCount = user.PostCount,
                FollowersCount = user.FollowersCount,
                FollowingCount = user.FollowingCount,
                AccountDescription = user.AccountDescription
            };
            return userViewModel;
        }
    }
}