﻿using SocialNetworkingServiceApp.API.ViewModels;
using SocialNetworkingServiceApp.Domain.Category;

namespace SocialNetworkingServiceApp.API.Mappers;

public class CategoryToCategoryViewModelMapper
{
    public static CategoryViewModel CategoryToCategoryViewModel(Category category)
    {
        var categoryViewModel = new CategoryViewModel()
        {
            CategoryId = category.CategoryId,
            CategoryName = category.CategoryName,
            IconHref = category.IconHref
        };
        return categoryViewModel;
    }
}