﻿using SocialNetworkingServiceApp.API.ViewModels;
using SocialNetworkingServiceApp.Domain.Group;

namespace SocialNetworkingServiceApp.API.Mappers;

public static class GroupToGroupViewModelMapper
{
    public static GroupViewModel GroupToGroupViewModel(Group group)
    {
        return new GroupViewModel()
        {
            GroupId = group.GroupId,
            AdminId = group.AdminId,
            GroupName = group.GroupName,
            GroupDescription = group.GroupDescription,
            GroupType = group.GroupType,
            ImageHref = group.ImageHref,
            MembersCount = group.MembersCount,
            PostCount = group.PostCount,
            Admin = new GroupAdminViewModel()
            {
                NickName = group.Admin.NickName,
                FirstName = group.Admin.FirstName,
                LastName = group.Admin.LastName,
                Gender = group.Admin.Gender
            }
        };
    }
}