﻿using SocialNetworkingServiceApp.API.ViewModels;
using SocialNetworkingServiceApp.Domain.Post;

namespace SocialNetworkingServiceApp.API.Mappers
{
    public class PostToPostViewModelMapper
    {
        public static PostViewModel PostToPostViewModel(Post post)
        {
            var postViewModel = new PostViewModel
            {
                PostId = post.PostId,
                UserId = post.UserId,
                PostBody = post.PostBody,
                CreationDate = post.CreationDate,
                EditionDate = post.EditionDate,
                LikesCount = post.LikesCount,
                CommentsCount = post.CommentsCount,
                ReactionsCount = post.ReactionsCount,
            };
            return postViewModel;
        }
    }
}