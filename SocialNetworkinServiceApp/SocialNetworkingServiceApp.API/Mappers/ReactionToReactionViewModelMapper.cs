﻿using SocialNetworkingServiceApp.API.ViewModels;
using SocialNetworkingServiceApp.Domain.Reaction;

namespace SocialNetworkingServiceApp.API.Mappers;

public class ReactionToReactionViewModelMapper
{
    public static ReactionViewModel ReactionToReactionViewModel(Reaction reaction)
    {
        var reactionViewModel = new ReactionViewModel()
        {
            ReactionName = reaction.ReactionName,
            IconHref = reaction.IconHref
        };
        return reactionViewModel;
    }
}