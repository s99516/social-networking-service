﻿using SocialNetworkingServiceApp.API.ViewModels;
using SocialNetworkingServiceApp.Domain.UserRelation;

namespace SocialNetworkingServiceApp.API.Mappers;

public class UserRelationToUserRelationViewModelMapper
{
    public static UserRelationViewModel UserRelationToUserRelationViewModel(UserRelation userRelation)
    {
        var userRelationViewModel = new UserRelationViewModel
        {
            UserRelationId = userRelation.UserRelationId,
            RelatedUserId = userRelation.RelatedUserId,
            RelatingUserId = userRelation.RelatingUserId,
            FollowDate = userRelation.FollowDate
        };
        return userRelationViewModel;
    }
}