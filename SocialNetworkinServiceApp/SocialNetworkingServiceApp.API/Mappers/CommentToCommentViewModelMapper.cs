﻿using SocialNetworkingServiceApp.API.ViewModels;
using SocialNetworkingServiceApp.Domain.Comment;

namespace SocialNetworkingServiceApp.API.Mappers;

public class CommentToCommentViewModelMapper
{
    public static CommentViewModel CommentToCommentViewModel(Comment comment)
    {
        var commentViewModel = new CommentViewModel()
        {
            PostId = comment.PostId,
            UserId = comment.UserId,
            UserNickName = comment.UserNickName,
            CommentBody = comment.CommentBody,
            CommentDateTime = comment.CommentDateTime
        };
        return commentViewModel;
    }
}