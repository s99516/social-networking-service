﻿using System.ComponentModel.DataAnnotations;

namespace SocialNetworkingServiceApp.API.BindingModels
{
    public class CreatePost
    {
        [Required]
        [Display(Name = "UserId")]
        public int UserId { get; set; }
        
        [Required]
        [Display(Name = "PostBody")]
        public string PostBody { get; set; }
    }
}