﻿using System;
using System.ComponentModel.DataAnnotations;
using SocialNetworkingServiceApp.Common.Enums;

namespace SocialNetworkingServiceApp.API.BindingModels
{
    public class EditUser
    {
        [Required]
        [Display(Name = "NickName")]
        public string NickName { get; set; }

        [Required]
        [Display(Name = "FirstName")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "LastName")]
        public string LastName { get; set; }

        [Required] [Display(Name = "Gender")] public Gender Gender { get; set; }

        [Required] [Display(Name = "Email")] public string Email { get; set; }

        [Required]
        [Display(Name = "BirthDate")]
        public DateTime BirthDate { get; set; }
    }
}