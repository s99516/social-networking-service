﻿using System.ComponentModel.DataAnnotations;

namespace SocialNetworkingServiceApp.API.BindingModels;

public class EditComment
{
    [Required]
    [Display(Name = "CommentBody")]
    public string CommentBody { get; set; }
}