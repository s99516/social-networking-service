﻿using System.ComponentModel.DataAnnotations;

namespace SocialNetworkingServiceApp.API.BindingModels;

public class CreatePostReactionUser
{
    [Required]
    [Display(Name = "PostId")]
    public int PostId { get; set; }
    [Required]
    [Display(Name = "ReactionId")]
    public int ReactionId { get; set; }
    [Required]
    [Display(Name = "UserId")]
    public int UserId { get; set; }
}