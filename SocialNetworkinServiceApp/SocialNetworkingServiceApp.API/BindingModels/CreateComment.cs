﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SocialNetworkingServiceApp.API.BindingModels
{
    public class CreateComment
    {
        [Required]
        [Display(Name = "PostId")]
        public int PostId { get; set; }
        [Required]
        [Display(Name = "UserId")]
        public int UserId { get; set; }

        [Required]
        [Display(Name="UserNickName")]
        public string UserNickName { get; set; }
        [Required]
        [Display(Name = "CommentBody")]
        public string CommentBody { get; set; }

        [Display(Name = "ParentCommentId")]
        public int ParentCommentId { get; set; }
    }
}