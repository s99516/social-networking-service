﻿using System.ComponentModel.DataAnnotations;

namespace SocialNetworkingServiceApp.API.BindingModels;

public class EditPost
{
    [Required]
    [Display(Name = "PostBody")]
    public string PostBody { get; set; }
}