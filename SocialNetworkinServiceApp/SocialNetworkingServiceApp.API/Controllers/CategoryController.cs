﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SocialNetworkingServiceApp.API.Mappers;
using SocialNetworkingServiceApp.DATA.SQL;
using SocialNetworkingServiceApp.IData.Category;
using SocialNetworkingServiceApp.IServices.Category;

namespace SocialNetworkingServiceApp.API.Controllers;

[ApiVersion("1.0")]
[Route("api/v{version:apiVersion}/category")]
public class CategoryController : Controller
{
    private readonly SocialNetworkingServiceAppDbContext _dbContext;
    private readonly ICategoryService _categoryService;

    public CategoryController(SocialNetworkingServiceAppDbContext dbContext, ICategoryService categoryService)
    {
        _dbContext = dbContext;
        _categoryService = categoryService;
    }

    [HttpGet(Name = "GetCategories")]
    public async Task<IActionResult> GetCategories()
    {
        var categoryList = await _categoryService.GetCategories();
        return categoryList != null
            ? Ok(categoryList.Select(CategoryToCategoryViewModelMapper.CategoryToCategoryViewModel))
            : NotFound();
    }
}