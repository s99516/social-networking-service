﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SocialNetworkingServiceApp.API.Mappers;
using SocialNetworkingServiceApp.API.Validation;
using SocialNetworkingServiceApp.DATA.SQL;
using SocialNetworkingServiceApp.IServices.Requests;
using SocialNetworkingServiceApp.IServices.User;

namespace SocialNetworkingServiceApp.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/post")]
    public class PostController : Controller
    {
        private readonly IPostService _postService;

        public PostController(IPostService postService)
        {
            _postService = postService;
        }

        // GET
        [HttpGet("{postId:min(1)}", Name = "GetPostById")]
        public async Task<IActionResult> GetPostById(int postId)
        {
            var post = await _postService.GetPostById(postId);
            if (post != null) return Ok(PostToPostViewModelMapper.PostToPostViewModel(post));
            return NotFound();
        }

        [ValidateModel]
        public async Task<IActionResult> Post([FromBody] CreatePost createPost)
        {
            var post = await _postService.CreatePost(createPost);
            return Created(post.PostId.ToString(), PostToPostViewModelMapper.PostToPostViewModel(post));
        }

        [ValidateModel]
        [HttpPatch("edit/{postId:min(1)}", Name = "EditPost")]
        public async Task<IActionResult> EditPost([FromBody] EditPost editPost, int postId)
        {
            await _postService.EditPost(editPost, postId);
            return NoContent();
        }

        [HttpGet("get-newest-posts", Name = "GetNewestPosts")]
        public async Task<IActionResult> GetNewestPosts()
        {
            var postList = await _postService.GetNewestPosts();
            return postList != null ? Ok(postList.Select(PostToPostViewModelMapper.PostToPostViewModel)) : NotFound();
        }
        
        [HttpGet("whole/{postId:min(1)}", Name = "GetWholePostById")]
        public async Task<IActionResult> GetWholePostById(int postId)
        {
            var post = await _postService.GetWholePost(postId);
            if (post != null) return Ok(post);
            return NotFound();
        }
    }
}