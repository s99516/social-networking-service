﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SocialNetworkingServiceApp.API.Mappers;
using SocialNetworkingServiceApp.API.Validation;
using SocialNetworkingServiceApp.DATA.SQL;
using SocialNetworkingServiceApp.IServices.Requests;
using SocialNetworkingServiceApp.IServices.User;

namespace SocialNetworkingServiceApp.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/user")]
    public class UserController : Controller
    {
        private readonly SocialNetworkingServiceAppDbContext _dbContext;
        private readonly IUserService _userService;

        public UserController(SocialNetworkingServiceAppDbContext dbContext, IUserService userService)
        {
            _dbContext = dbContext;
            _userService = userService;
        }

        // GET
        [HttpGet("{userId:min(1)}", Name = "GetUserById")]
        public async Task<IActionResult> GetUserById(int userId)
        {
            var user = await _userService.GetUserById(userId);
            if (user != null) return Ok(UserToUserViewModelMapper.UserToUserViewModel(user));
            return NotFound();
            //return user != null ? Ok(UserToUserViewModelMapper.UserToUserViewModel(user)) : NotFound();
        }


        [HttpGet("nickName/{nickName}", Name = "GetUserByNickName")]
        public async Task<IActionResult> GetUserByNickName(string nickName)
        {
            var user = await _userService.GetUserByNickName(nickName);
            if (user != null) return Ok(UserToUserViewModelMapper.UserToUserViewModel(user));
            return NotFound();
        }

        [ValidateModel]
        public async Task<IActionResult> Post([FromBody] CreateUser createUser)
        {
            var user = await _userService.CreateUser(createUser);
            return Created(user.UserId.ToString(), UserToUserViewModelMapper.UserToUserViewModel(user));
        }

        [ValidateModel]
        [HttpPatch("edit/{userId:min(1)}", Name = "EditUser")]
        public async Task<IActionResult> EditUser([FromBody] EditUser editUser, int userId)
        {
            await _userService.EditUser(editUser, userId);
            return NoContent();
        }

        [HttpGet("getRelatedUsers/{userId:min(1)}", Name = "GetRelatedUsers")]
        public async Task<IActionResult> GetRelatedUsers(int userId)
        {
            var relatedUsersList=  await _userService.GetRelatedUsersOfUserByUserId(userId);
            if (relatedUsersList != null)
            {
                return Ok(relatedUsersList.Select(UserToUserViewModelMapper.UserToUserViewModel));
            }
            return NotFound();
        }
        
        [HttpGet("getRelatingUsers/{userId:min(1)}", Name = "GetRelatingUsers")]
        public async Task<IActionResult> GetRelatingUsers(int userId)
        {
            var relatingUsersList=  await _userService.GetRelatingUsersOfUserByUserId(userId);
            if (relatingUsersList != null)
            {
                return Ok(relatingUsersList.Select(UserToUserViewModelMapper.UserToUserViewModel));
            }
            return NotFound();
        }

        [HttpDelete("deleteUser/{userId:min(1)}", Name = "DeleteUser")]
        public async Task<IActionResult> DeleteUser(int userId)
        {
            await _userService.DeleteUser(userId);
            return Accepted();
        }

        [HttpPost("add-user-to-group/{userId},{groupId}")]
        public async Task<IActionResult> AddUserToGroup(int userId, int groupId)
        {
            await _userService.AddUserToGroup(userId, groupId);
            return Ok();
        }
    }
}