﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Metadata.Conventions;
using SocialNetworkingServiceApp.API.Mappers;
using SocialNetworkingServiceApp.API.Validation;
using SocialNetworkingServiceApp.DATA.SQL;
using SocialNetworkingServiceApp.IServices.Comment;
using SocialNetworkingServiceApp.IServices.Requests;

namespace SocialNetworkingServiceApp.API.Controllers;

[ApiVersion("1.0")]
[Route("api/v{version:apiVersion}/comment")]
public class CommentController : Controller
{
    private readonly ICommentService _commentService;

    public CommentController(ICommentService commentService)
    {
        _commentService = commentService;
    }

    //GET
    [HttpGet("{commentId:min(1)}", Name = "GetCommentById")]
    public async Task<IActionResult> GetCommentById(int commentId)
    {
        var comment = await _commentService.GetCommentById(commentId);
        if (comment != null) return Ok(CommentToCommentViewModelMapper.CommentToCommentViewModel(comment));
        return NotFound();
    }

    [HttpGet("getCommentsByPostId/{postId:min(1)}", Name = "GetCommentsByPostId")]
    public async Task<IActionResult> GetCommentsByPostId(int postId)
    {
        var commentsList = await _commentService.GetCommentsByPostId(postId);
        if (commentsList != null)
        {
            return Ok(commentsList.Select(CommentToCommentViewModelMapper.CommentToCommentViewModel));
        }
        return NotFound();
    }

    [HttpGet("getCommentsByParentCommentId/{parentCommentId:min(1)}", Name = "GetCommentsByParentCommentId")]
    public async Task<IActionResult> GetCommentsByParentCommentId(int parentCommentId)
    {
        var subCommentsList = await _commentService.GetSubCommentsByParentCommentId(parentCommentId);
        if (subCommentsList != null)
        {
            return Ok(subCommentsList.Select(CommentToCommentViewModelMapper.CommentToCommentViewModel));
        }

        return NotFound();
    }

    [ValidateModel]
    public async Task<IActionResult> Post([FromBody] CreateComment createComment)
    {
        var comment = await _commentService.CreateComment(createComment);
        return Created(comment.CommentId.ToString(),
            CommentToCommentViewModelMapper.CommentToCommentViewModel(comment));
    }

    [ValidateModel]
    [HttpPatch("edit/{commentId:min(1)}", Name = "EditComment")]
    public async Task<IActionResult> EditComment([FromBody] EditComment editComment, int commentId)
    {
        await _commentService.EditComment(editComment, commentId);
        return NoContent();
    }

    [HttpDelete("delete/{commentId:min(1)}")]
    public async Task<IActionResult> DeleteComment(int commentId)
    {
        await _commentService.DeleteComment(commentId);
        return Accepted();
    }
}