﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.OpenApi.Writers;
using SocialNetworkingServiceApp.API.Mappers;
using SocialNetworkingServiceApp.IServices.Reaction;

namespace SocialNetworkingServiceApp.API.Controllers;

[ApiVersion("1.0")]
[Route("api/v{version:apiVersion}/reaction")]
public class ReactionController : Controller
{
    private readonly IReactionService _reactionService;

    public ReactionController(IReactionService reactionService)
    {
        _reactionService = reactionService;
    }

    [HttpGet("get-reactions", Name = "GetReactions")]
    public async Task<IActionResult> GetReactions()
    {
        var reactionsList = await _reactionService.GetReactions();
        
        return reactionsList != null ? Ok(reactionsList.Select(ReactionToReactionViewModelMapper.ReactionToReactionViewModel)) : NotFound();
    }

    [HttpDelete("delete-reaction/{reactionId}")]
    public async Task<IActionResult> DeleteReaction(int reactionId)
    {
       await _reactionService.DeleteReaction(reactionId);
       return Accepted();
    }
}