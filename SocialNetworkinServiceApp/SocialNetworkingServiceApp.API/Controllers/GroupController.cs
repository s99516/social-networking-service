﻿using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SocialNetworkingServiceApp.API.Mappers;
using SocialNetworkingServiceApp.IData.Group;
using SocialNetworkingServiceApp.IServices.Group;
using SocialNetworkingServiceApp.IServices.Requests;

namespace SocialNetworkingServiceApp.API.Controllers;

[ApiVersion("1.0")]
[Route("api/v{version:apiVersion}/group")]
public class GroupController : Controller
{
    private readonly IGroupService _groupService;

    public GroupController(IGroupService groupService)
    {
        _groupService = groupService;
    }

    [HttpGet("get-groups-by-name/{name}", Name = "GetGroupsByName")]
    public async Task<IActionResult> GetGroupsByName(string name)
    {
        var groupList = await _groupService.GetGroupsByName(name);
        return groupList != null ? Ok(groupList.Select(GroupToGroupViewModelMapper.GroupToGroupViewModel)) : NotFound();
    }

    [HttpPost("create-group")]
    public async Task<IActionResult> CreateGroupAsync([FromBody] CreateGroup createGroup)
    {
        var group = await _groupService.CreateGroup(createGroup);
        return Created(group.GroupId.ToString(), group);
    }

    [HttpDelete("delete-group-by-id/{groupId}", Name = "DeleteGroupById")]
    public async Task<IActionResult> DeleteGroupById(int groupId)
    {
        return await _groupService.DeleteGroup(groupId) ? Accepted() : NoContent();
    }
    
    [HttpPatch("edit-group/{groupId}")]
    public async Task<IActionResult> EditGroup([FromBody] EditGroup editGroup, int groupId)
    {
        await _groupService.EditGroup(editGroup, groupId);
        return NoContent();
    }
}