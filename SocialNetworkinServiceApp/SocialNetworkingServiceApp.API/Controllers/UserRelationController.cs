﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SocialNetworkingServiceApp.API.Mappers;
using SocialNetworkingServiceApp.DATA.SQL;
using SocialNetworkingServiceApp.IServices.UserRelation;

namespace SocialNetworkingServiceApp.API.Controllers;

[ApiVersion("1.0")]
[Route("api/v{version:ApiVersion}/userRelation")]
public class UserRelationController : Controller
{
    private readonly SocialNetworkingServiceAppDbContext _dbContext;
    private readonly IUserRelationService _userRelationService;

    public UserRelationController(SocialNetworkingServiceAppDbContext dbContext, IUserRelationService userRelationService)
    {
        _dbContext = dbContext;
        _userRelationService = userRelationService;
    }

    [HttpGet("{userRelationId:min(1)}", Name = "GetUserRelationById")]
    public async Task<IActionResult> GetUserRelationById(int userRelationId)
    {
        var userRelation = await _userRelationService.GetUserRelationById(userRelationId);
        if (userRelation != null)
            return Ok(UserRelationToUserRelationViewModelMapper.UserRelationToUserRelationViewModel(userRelation));
        return NotFound();
    }

    [HttpGet("getRelatedUsersId/{userId:min(1)}", Name = "GetRelatedUsersIdOfUser")]
    public async Task<IActionResult> GetRelatedUsersId(int userId)
    {
        var relatedUsersId = await _userRelationService.GetRelatedUsersIdsOfUserByUserId(userId);
        return relatedUsersId != null ? Ok(relatedUsersId) : NotFound();
    }
    
    [HttpGet("getRelatingUsersId/{userId:min(1)}", Name = "GetRelatingUsersIdOfUser")]
    public async Task<IActionResult> GetRelatingUsersId(int userId)
    {
        var relatingUsersId = await _userRelationService.GetRelatingUsersIdsOfUserByUserId(userId);
        return relatingUsersId != null ? Ok(relatingUsersId) : NotFound();
    }
}