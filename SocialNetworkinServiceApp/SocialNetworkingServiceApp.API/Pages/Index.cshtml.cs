﻿using Microsoft.AspNetCore.Mvc.RazorPages;

namespace SocialNetworkingServiceApp.API.Pages
{
    public class Index : PageModel
    {
        public string Message { get; set; }

        public void OnGet()
        {
            Message = "Hello";
        }
    }
}