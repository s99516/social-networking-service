﻿using FluentValidation;
using EditUser = SocialNetworkingServiceApp.API.BindingModels.EditUser;

namespace SocialNetworkingServiceApp.API.Validation
{
    public class EditUserValidation : AbstractValidator<EditUser>
    {
        public EditUserValidation()
        {
            RuleFor(x => x.NickName).NotNull();
            RuleFor(x => x.FirstName).NotNull();
            RuleFor(x => x.LastName).NotNull();
            RuleFor(x => x.Gender).NotNull();
            RuleFor(x => x.Email).NotNull();
            RuleFor(x => x.BirthDate).NotNull();
        }
    }
}