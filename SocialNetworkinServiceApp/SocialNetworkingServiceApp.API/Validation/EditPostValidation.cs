﻿using FluentValidation;
using SocialNetworkingServiceApp.API.BindingModels;

namespace SocialNetworkingServiceApp.API.Validation;

public class EditPostValidation : AbstractValidator<EditPost>
{
    public EditPostValidation()
    {
        RuleFor(x => x.PostBody).NotNull();
    }
}