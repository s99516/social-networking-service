﻿using FluentValidation;
using SocialNetworkingServiceApp.API.BindingModels;

namespace SocialNetworkingServiceApp.API.Validation
{
    public class CreateUserValidation : AbstractValidator<CreateUser>
    {
        public CreateUserValidation()
        {
            RuleFor(x => x.NickName).NotNull();
            RuleFor(x => x.FirstName).NotNull();
            RuleFor(x => x.LastName).NotNull();
            RuleFor(x => x.Gender).NotNull();
            RuleFor(x => x.Email).NotNull();
            RuleFor(x => x.Password).NotNull();
            RuleFor(x => x.BirthDate).NotNull();
        }
    }
}