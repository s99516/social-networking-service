﻿using FluentValidation;
using SocialNetworkingServiceApp.API.BindingModels;

namespace SocialNetworkingServiceApp.API.Validation
{
    public class CreateCommentValidation : AbstractValidator<CreateComment>
    {
        public CreateCommentValidation()
        {
            RuleFor(x => x.PostId).NotNull();
            RuleFor(x => x.UserId).NotNull();
            RuleFor(x => x.UserNickName).NotNull();
            RuleFor(x => x.CommentBody).NotNull();
        }
    }
}