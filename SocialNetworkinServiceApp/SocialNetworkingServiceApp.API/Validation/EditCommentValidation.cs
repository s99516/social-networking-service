﻿using FluentValidation;
using SocialNetworkingServiceApp.API.BindingModels;

namespace SocialNetworkingServiceApp.API.Validation;

public class EditCommentValidation : AbstractValidator<EditComment>
{
    public EditCommentValidation()
    {
        RuleFor(x => x.CommentBody).NotNull();
    }
}