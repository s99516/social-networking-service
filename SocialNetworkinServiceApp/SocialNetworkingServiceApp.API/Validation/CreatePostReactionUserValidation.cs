﻿using FluentValidation;
using SocialNetworkingServiceApp.API.BindingModels;

namespace SocialNetworkingServiceApp.API.Validation;

public class CreatePostReactionUserValidation : AbstractValidator<CreatePostReactionUser>
{
    public CreatePostReactionUserValidation()
    {
        RuleFor(x => x.PostId).NotNull();
        RuleFor(x => x.ReactionId).NotNull();
        RuleFor(x => x.UserId).NotNull();
    }
}