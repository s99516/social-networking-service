﻿using FluentValidation;
using CreatePost = SocialNetworkingServiceApp.API.BindingModels.CreatePost;

namespace SocialNetworkingServiceApp.API.Validation
{
    public class CreatePostValidation : AbstractValidator<CreatePost>
    {
        public CreatePostValidation()
        {
            RuleFor(x => x.UserId).NotNull();
            RuleFor(x => x.PostBody).NotNull();
        }
    }
}