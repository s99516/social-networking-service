﻿using System.Reflection.Metadata;
using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using SocialNetworkingServiceApp.Common.Enums;
using SocialNetworkingServiceApp.DATA.SQL.Post;
using SocialNetworkingServiceApp.DATA.SQL.User;
using SocialNetworkingServiceApp.IData.Post;
using SocialNetworkingServiceApp.IData.User;
using Xunit;
using static System.DateTimeKind;

namespace SocialNetworkingServiceApp.DATA.SQL.Tests.Post;

public class PostRepositoryTest
{
    public IConfiguration Configuration { get; }
    private SocialNetworkingServiceAppDbContext _context;
    private IPostRepository _postRepository;
    private IUserRepository _userRepository;
    

    public PostRepositoryTest()
    {
        var optionsBuilder = new DbContextOptionsBuilder<SocialNetworkingServiceAppDbContext>();
        optionsBuilder.UseMySQL("server=localhost;userid=root;pwd=rafal69;port=3306;database=social_networking_test_db;");
        _context = new SocialNetworkingServiceAppDbContext(optionsBuilder.Options);
        _context.Database.EnsureDeleted();
        _context.Database.EnsureCreated();
        _postRepository = new PostRepository(_context);
        _userRepository = new UserRepository(_context);
    }


    [Fact]
    public async Task AddPost_Return_Correct_Response()
    {
        //arrange
        var user = new Domain.User.User("nick", "firstName", "lastName", Gender.Male, "email", "secret", DateTime.UtcNow.AddYears(-17));
        var post = new Domain.Post.Post(1, "Test post body");
        var userId = await _userRepository.AddUser(user);

        //act
        var postId = await _postRepository.AddPost(post);
        var createdPost = await _context.Post.FirstOrDefaultAsync(x => x.PostId == postId);
        
        //assert
        Assert.NotNull(createdPost);

        //clear
        var createdUser = await _context.User.FirstOrDefaultAsync(x => x.UserId == userId);

        _context.Post.Remove(createdPost);
        _context.User.Remove(createdUser);
        await _context.SaveChangesAsync();
    }
}