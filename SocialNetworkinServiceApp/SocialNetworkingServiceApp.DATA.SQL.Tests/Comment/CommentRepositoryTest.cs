﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SocialNetworkingServiceApp.Common.Enums;
using SocialNetworkingServiceApp.DATA.SQL.Comment;
using SocialNetworkingServiceApp.DATA.SQL.Post;
using SocialNetworkingServiceApp.DATA.SQL.User;
using SocialNetworkingServiceApp.IData.Comment;
using SocialNetworkingServiceApp.IData.Post;
using SocialNetworkingServiceApp.IData.User;
using Xunit;

namespace SocialNetworkingServiceApp.DATA.SQL.Tests.Comment;

public class CommentRepositoryTest
{
    private SocialNetworkingServiceAppDbContext _context;
    private ICommentRepository _commentRepository;
    private IPostRepository _postRepository;
    private IUserRepository _userRepository;

    public CommentRepositoryTest()
    {
        var optionsBuilder = new DbContextOptionsBuilder<SocialNetworkingServiceAppDbContext>();
        optionsBuilder.UseMySQL(
            "server=localhost;userid=root;pwd=rafal69;port=3306;database=social_networking_test_db;");
        _context = new SocialNetworkingServiceAppDbContext(optionsBuilder.Options);
        _context.Database.EnsureDeleted();
        _context.Database.EnsureCreated();
        _commentRepository = new CommentRepository(_context);
        _postRepository = new PostRepository(_context);
        _userRepository = new UserRepository(_context);
    }

    [Fact]
    public async Task AddComment_Return_Correct_Response()
    {
        //arrange
        var user = new Domain.User.User("nick", "firstName", "lastName", Gender.Male, "email", "secret", DateTime.UtcNow.AddYears(-17));
        var post = new Domain.Post.Post(1, "Test post body");
        var userId = await _userRepository.AddUser(user);
        var postId = await _postRepository.AddPost(post);
        var comment = new Domain.Comment.Comment(null, postId, userId, user.NickName, "Comment body");
        
        //act
        var commentId = await _commentRepository.AddComment(comment);
        var createdComment = await _context.Comment.FirstOrDefaultAsync(x => x.CommentId == commentId);
        
        //assert
        Assert.NotNull(createdComment);
        
        //clear
        var createdUser = await _context.User.FirstOrDefaultAsync(x => x.UserId == userId);
        var createdPost = await _context.Post.FirstOrDefaultAsync(x => x.PostId == postId);

        _context.Comment.Remove(createdComment);
        _context.Post.Remove(createdPost);
        _context.User.Remove(createdUser);
        await _context.SaveChangesAsync();
    }
}