﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using SocialNetworkingServiceApp.Common.Enums;
using SocialNetworkingServiceApp.DATA.SQL.User;
using SocialNetworkingServiceApp.IData.User;
using Xunit;

namespace SocialNetworkingServiceApp.DATA.SQL.Tests.User;

public class UserRepositoryTest
{
    public IConfiguration Configuration { get; }
    private SocialNetworkingServiceAppDbContext _context;
    private IUserRepository _userRepository;

    public UserRepositoryTest()
    {
        var optionsBuilder = new DbContextOptionsBuilder<SocialNetworkingServiceAppDbContext>();
        optionsBuilder.UseMySQL("server=localhost;userid=root;pwd=rafal69;port=3306;database=social_networking_test_db;");
        _context = new SocialNetworkingServiceAppDbContext(optionsBuilder.Options);
        _context.Database.EnsureDeleted();
        _context.Database.EnsureCreated();
        _userRepository = new UserRepository(_context);
    }

    [Fact]
    public async Task AddUser_Return_Correct_Response()
    {
        //arrange
        var user = new Domain.User.User("nick", "firstName", "lastName", Gender.Male, "email", "secret", DateTime.UtcNow.AddYears(-17));
        var userId = await _userRepository.AddUser(user);
        
        //act
        var createdUser = await _context.User.FirstOrDefaultAsync(x => x.UserId == userId);
        //assert
        Assert.NotNull(createdUser);

        //clear
        _context.User.Remove(createdUser);
        await _context.SaveChangesAsync();
    }
    
}