﻿namespace SocialNetworkingServiceApp.IServices.Category;

public interface ICategoryService
{
    Task<IEnumerable<Domain.Category.Category>> GetCategories();
}