﻿namespace SocialNetworkingServiceApp.IServices.Reaction;

public interface IReactionService
{
    Task<IEnumerable<Domain.Reaction.Reaction>> GetReactions();
    Task DeleteReaction(int userId);
}