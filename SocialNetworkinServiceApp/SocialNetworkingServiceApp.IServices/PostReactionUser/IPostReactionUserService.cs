﻿using SocialNetworkingServiceApp.IServices.Requests;

namespace SocialNetworkingServiceApp.IServices.PostReactionUser;

public interface IPostReactionUserService
{
    Task<Domain.PostReactionUser.PostReactionUser> CreatePostReactionUser(CreatePostReactionUser createPostReactionUser);
}