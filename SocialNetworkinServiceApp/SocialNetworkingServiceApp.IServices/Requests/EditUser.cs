﻿using System;
using SocialNetworkingServiceApp.Common.Enums;

namespace SocialNetworkingServiceApp.IServices.Requests
{
    public class EditUser
    {
        public string NickName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Gender Gender { get; set; }
        public string Email { get; set; }
        public DateTime BirthDate { get; set; }
    }
}