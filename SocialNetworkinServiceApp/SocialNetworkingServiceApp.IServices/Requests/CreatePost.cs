﻿using System;

namespace SocialNetworkingServiceApp.IServices.Requests
{
    public class CreatePost
    {
        public int UserId { get; set; }
        public string PostBody { get; set; }
    }
}