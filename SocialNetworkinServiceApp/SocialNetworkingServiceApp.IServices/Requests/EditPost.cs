﻿namespace SocialNetworkingServiceApp.IServices.Requests;

public class EditPost
{
    public string PostBody { get; set; }
}