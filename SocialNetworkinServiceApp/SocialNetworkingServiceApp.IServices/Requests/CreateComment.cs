﻿namespace SocialNetworkingServiceApp.IServices.Requests
{
    public class CreateComment
    {
        public int? ParentCommentId { get; set; }
        public int PostId { get; set; }
        public int UserId { get; set; }
        public string UserNickName { get; set; }
        public string CommentBody { get; set; }
    }
}