﻿using SocialNetworkingServiceApp.Common.Enums;

namespace SocialNetworkingServiceApp.IServices.Requests;

public class CreateGroup
{
    public int  AdminId { get; set; }
    public string GroupName { get; set; }
    public string GroupDescription { get; set; }
    public GroupType GroupType { get; set; }
    public string ImageHref { get; set; }
}