﻿namespace SocialNetworkingServiceApp.IServices.Requests;

public class CreatePostReactionUser
{
    public int PostId { get; set; }
    public int ReactionId { get; set; }
    public int UserId { get; set; }
}