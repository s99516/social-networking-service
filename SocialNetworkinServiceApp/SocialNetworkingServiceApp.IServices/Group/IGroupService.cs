﻿using SocialNetworkingServiceApp.IServices.Requests;

namespace SocialNetworkingServiceApp.IServices.Group;

public interface IGroupService
{
    Task<IEnumerable<Domain.Group.Group>> GetGroupsByName(string name);
    Task<Domain.Group.Group> CreateGroup(CreateGroup createGroup);
    Task<bool> DeleteGroup(int groupId);
    Task EditGroup(EditGroup editGroup, int gruopId);
}