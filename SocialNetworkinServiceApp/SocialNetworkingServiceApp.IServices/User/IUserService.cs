﻿using System.Threading.Tasks;
using SocialNetworkingServiceApp.IServices.Requests;

namespace SocialNetworkingServiceApp.IServices.User
{
    public interface IUserService
    {
        Task<Domain.User.User> GetUserById(int userId);
        Task<Domain.User.User> GetUserByNickName(string nickName);
        Task<Domain.User.User> CreateUser(CreateUser createUser);
        Task EditUser(EditUser editUser, int userId);
        Task<IEnumerable<Domain.User.User>> GetRelatedUsersOfUserByUserId(int userId);
        Task<IEnumerable<Domain.User.User>> GetRelatingUsersOfUserByUserId(int userId);
        Task DeleteUser(int userId);
        Task AddUserToGroup(int userId, int groupid);
    }
}