﻿namespace SocialNetworkingServiceApp.IServices.UserRelation;

public interface IUserRelationService
{
    Task<Domain.UserRelation.UserRelation> GetUserRelationById(int userRelationId);
    Task<IEnumerable<int>> GetRelatedUsersIdsOfUserByUserId(int userId);
    Task<IEnumerable<int>> GetRelatingUsersIdsOfUserByUserId(int userId);
}