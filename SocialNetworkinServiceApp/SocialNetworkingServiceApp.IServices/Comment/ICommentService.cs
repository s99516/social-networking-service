﻿using SocialNetworkingServiceApp.IServices.Requests;

namespace SocialNetworkingServiceApp.IServices.Comment;

public interface ICommentService
{
    Task<Domain.Comment.Comment> GetCommentById(int commentId);
    Task<IEnumerable<SocialNetworkingServiceApp.Domain.Comment.Comment>> GetCommentsByPostId(int postId);

    Task<IEnumerable<SocialNetworkingServiceApp.Domain.Comment.Comment>> GetSubCommentsByParentCommentId(
        int parentCommentId);
    Task<Domain.Comment.Comment> CreateComment(CreateComment createComment);
    Task EditComment(EditComment editComment, int commentId);
    Task DeleteComment(int commentId);
}