﻿using System.Threading.Tasks;
using SocialNetworkingServiceApp.Domain.Post;
using SocialNetworkingServiceApp.IServices.Requests;

namespace SocialNetworkingServiceApp.IServices.User
{
    public interface IPostService
    {
        Task<Post> GetPostById(int postId);
        Task<Post> CreatePost(CreatePost createPost);
        Task EditPost(EditPost editPost, int postId);
        Task<IEnumerable<Domain.Post.Post>> GetNewestPosts();
        
        Task<Domain.Post.Post> GetWholePost(int postId);
    }
}