﻿namespace SocialNetworkingServiceApp.Common.Extensions
{
    public static class ParityExtension
    {
        public static bool IsEven(int value)
        {
            return value % 2 == 0;
        }

        public static bool IsOdd(int value)
        {
            return value % 2 != 0;
        }
    }
}