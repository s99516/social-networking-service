﻿using System;
using System.Collections.Generic;

namespace SocialNetworkingServiceApp.Common.Extensions
{
    public static class RandomUtils<T>
    {
        private static readonly Random _random = new();

        public static T Choice<T>(params T[] elements)
        {
            var element = elements[_random.Next(0, elements.Length)];
            return element;
        }

        public static T ChoiceFromList<T>(List<T> elements)
        {
            var element = elements[_random.Next(0, elements.Count)];
            return element;
        }
    }
}