﻿namespace SocialNetworkingServiceApp.Common.Enums;

public enum GroupType
{
    Educational,
    Hobby,
    Work,
    Sport,
    Animals,
    Travel,
    Food,
    Other
}