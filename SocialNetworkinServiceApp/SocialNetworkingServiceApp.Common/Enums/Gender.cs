﻿namespace SocialNetworkingServiceApp.Common.Enums
{
    public enum Gender
    {
        Male,
        Female
    }
}