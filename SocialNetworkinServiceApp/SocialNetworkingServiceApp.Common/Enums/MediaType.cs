﻿namespace SocialNetworkingServiceApp.Common.Enums
{
    public enum MediaType
    {
        Image,
        Video,
        Audio
    }
}