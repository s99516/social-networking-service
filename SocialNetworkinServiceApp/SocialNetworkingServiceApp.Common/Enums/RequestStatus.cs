﻿namespace SocialNetworkingServiceApp.Common.Enums;

public enum RequestStatus
{
    Waiting,
    Accepted,
    Rejected,
}