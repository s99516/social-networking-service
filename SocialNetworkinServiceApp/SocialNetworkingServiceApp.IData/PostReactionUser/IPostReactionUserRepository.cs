﻿namespace SocialNetworkingServiceApp.IData.PostReactionUser;

public interface IPostReactionUserRepository
{
    Task<int> AddPostReactionUser(Domain.PostReactionUser.PostReactionUser postReactionUser);
}