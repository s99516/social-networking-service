﻿using System.Threading.Tasks;

namespace SocialNetworkingServiceApp.IData.User
{
    public interface IUserRepository
    {
        Task<Domain.User.User> GetUser(int userId);
        Task<Domain.User.User> GetUser(string nickName);
        Task<int> AddUser(Domain.User.User user);
        Task EditUser(Domain.User.User user);
        Task DeleteUser(int userId);
        Task AddUserToGroup(int userId, int groupId);
    }
}