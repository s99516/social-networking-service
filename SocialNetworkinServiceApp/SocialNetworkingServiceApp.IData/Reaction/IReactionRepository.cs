﻿namespace SocialNetworkingServiceApp.IData.Reaction;

public interface IReactionRepository
{
    Task<IEnumerable<Domain.Reaction.Reaction>> GetReactions();
    Task DeleteReaction(int reactionId);
}