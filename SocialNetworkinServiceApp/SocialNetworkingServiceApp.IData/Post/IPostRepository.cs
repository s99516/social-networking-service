﻿using System.Threading.Tasks;

namespace SocialNetworkingServiceApp.IData.Post
{
    public interface IPostRepository
    {
        Task<Domain.Post.Post> GetPost(int postId);
        Task<int> AddPost(Domain.Post.Post post);
        Task EditPost(Domain.Post.Post post);
        Task<IEnumerable<Domain.Post.Post>> GetNewestPosts();
        Task<Domain.Post.Post> GetWholePost(int postId);
    }
}