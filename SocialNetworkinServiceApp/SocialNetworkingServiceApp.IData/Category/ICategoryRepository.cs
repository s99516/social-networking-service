﻿namespace SocialNetworkingServiceApp.IData.Category;

public interface ICategoryRepository
{
    Task<IEnumerable<Domain.Category.Category>> GetCategories();
}