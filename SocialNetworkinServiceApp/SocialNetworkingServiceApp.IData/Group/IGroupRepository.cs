﻿namespace SocialNetworkingServiceApp.IData.Group;

public interface IGroupRepository
{
    Task<Domain.Group.Group> GetGroupById(int groupId);
    Task<IEnumerable<Domain.Group.Group>> GetGroupsByName(string name);
    Task<int> AddGroup(Domain.Group.Group group);
    Task<bool> DeleteGroup(int groupId);
    Task EditGroup(Domain.Group.Group @group);
    
}