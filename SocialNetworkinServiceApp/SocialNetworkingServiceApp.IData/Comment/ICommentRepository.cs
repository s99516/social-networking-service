﻿namespace SocialNetworkingServiceApp.IData.Comment;

public interface ICommentRepository
{
    Task<Domain.Comment.Comment> GetComment(int commentId);
    Task<IEnumerable<Domain.Comment.Comment>> GetCommentsByPostId(int postId);
    Task<IEnumerable<Domain.Comment.Comment>> GetCommentsByParentCommentId(int parentCommentId);
    Task<int> AddComment(Domain.Comment.Comment comment);
    Task EditComment(Domain.Comment.Comment comment);
    Task DeleteComment(int commentId);
}