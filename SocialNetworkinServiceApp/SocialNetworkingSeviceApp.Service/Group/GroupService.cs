﻿using SocialNetworkingServiceApp.IData.Group;
using SocialNetworkingServiceApp.IServices.Group;
using SocialNetworkingServiceApp.IServices.Requests;

namespace SocialNetworkingSeviceApp.Service.Group;

public class GroupService : IGroupService
{
    private readonly IGroupRepository _groupRepository;

    public GroupService(IGroupRepository groupRepository)
    {
        _groupRepository = groupRepository;
    }
    
    public async Task<IEnumerable<SocialNetworkingServiceApp.Domain.Group.Group>> GetGroupsByName(string name)
    {
        return await _groupRepository.GetGroupsByName(name);
    }

    public async Task<SocialNetworkingServiceApp.Domain.Group.Group> CreateGroup(CreateGroup createGroup)
    {
        var group = new SocialNetworkingServiceApp.Domain.Group.Group(
            createGroup.AdminId,
            createGroup.GroupName,
            createGroup.GroupDescription,
            createGroup.GroupType,
            createGroup.ImageHref);
        group.GroupId = await _groupRepository.AddGroup(group);
        return group;
    }

    public async Task<bool> DeleteGroup(int groupId)
    {
        return await _groupRepository.DeleteGroup(groupId);
    }

    public async Task EditGroup(EditGroup editGroup, int groupId)
    {
        var group = await _groupRepository.GetGroupById(groupId);
        group.EditGroup(
            editGroup.GroupName,
            editGroup.GroupDescription,
            editGroup.GroupType,
            editGroup.ImageHref);
        
        await _groupRepository.EditGroup(group);
    }
}