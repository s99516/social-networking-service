﻿using SocialNetworkingServiceApp.IData.UserRelation;
using SocialNetworkingServiceApp.IServices.UserRelation;

namespace SocialNetworkingSeviceApp.Service.UserRelation;

public class UserRelationService : IUserRelationService
{
    private readonly IUserRelationRepository _userRelationRepository;

    public UserRelationService(IUserRelationRepository userRelationRepository)
    {
        _userRelationRepository = userRelationRepository;
    }
    public Task<SocialNetworkingServiceApp.Domain.UserRelation.UserRelation> GetUserRelationById(int userRelationId)
    {
        return _userRelationRepository.GetUserRelationById(userRelationId);
    }

    public Task<IEnumerable<int>> GetRelatedUsersIdsOfUserByUserId(int userId)
    {
        return _userRelationRepository.GetRelatedUsersIdsOfUserByUserId(userId);
    }

    public Task<IEnumerable<int>> GetRelatingUsersIdsOfUserByUserId(int userId)
    {
        return _userRelationRepository.GetRelatingUsersIdsOfUserByUserId(userId);
    }
}