﻿using System.Threading.Tasks;
using SocialNetworkingServiceApp.Domain.Post;
using SocialNetworkingServiceApp.IData.Post;
using SocialNetworkingServiceApp.IServices.Requests;
using SocialNetworkingServiceApp.IServices.User;

namespace SocialNetworkingSeviceApp.Service.User
{
    public class PostService : IPostService
    {
        private readonly IPostRepository _postRepository;

        public PostService(IPostRepository postRepository)
        {
            _postRepository = postRepository;
        }

        public Task<SocialNetworkingServiceApp.Domain.Post.Post> GetPostById(int postId)
        {
            return _postRepository.GetPost(postId);
        }

        public async Task<SocialNetworkingServiceApp.Domain.Post.Post> CreatePost(CreatePost createPost)
        {
            var post = new SocialNetworkingServiceApp.Domain.Post.Post(createPost.UserId, createPost.PostBody);
            post.PostId = await _postRepository.AddPost(post);
            return post;
        }

        public async Task EditPost(EditPost editPost, int postId)
        {
            var post = await _postRepository.GetPost(postId);
            post.EditPost(editPost.PostBody);
            await _postRepository.EditPost(post);
        }

        public async Task<IEnumerable<Post>> GetNewestPosts()
        {
            return await _postRepository.GetNewestPosts();
        }

        public Task<Post> GetWholePost(int postId)
        {
            return _postRepository.GetWholePost(postId);
        }
    }
}