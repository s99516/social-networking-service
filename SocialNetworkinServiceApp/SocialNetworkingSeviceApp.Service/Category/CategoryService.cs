﻿using SocialNetworkingServiceApp.IData.Category;
using SocialNetworkingServiceApp.IServices.Category;

namespace SocialNetworkingSeviceApp.Service.Category;

public class CategoryService : ICategoryService
{
    private readonly ICategoryRepository _categoryRepository;

    public CategoryService(ICategoryRepository categoryRepository)
    {
        _categoryRepository = categoryRepository;
    }
    public Task<IEnumerable<SocialNetworkingServiceApp.Domain.Category.Category>> GetCategories()
    {
        return _categoryRepository.GetCategories();
    }
}