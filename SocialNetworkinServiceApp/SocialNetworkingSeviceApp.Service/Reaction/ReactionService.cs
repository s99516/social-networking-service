﻿using SocialNetworkingServiceApp.IData.Reaction;
using SocialNetworkingServiceApp.IServices.Reaction;

namespace SocialNetworkingSeviceApp.Service.Reaction;

public class ReactionService : IReactionService
{
    private readonly IReactionRepository _reactionRepository;

    public ReactionService(IReactionRepository reactionRepository)
    {
        _reactionRepository = reactionRepository;
    }

    public Task<IEnumerable<SocialNetworkingServiceApp.Domain.Reaction.Reaction>> GetReactions()
    {
        return _reactionRepository.GetReactions();
    }

    public async Task DeleteReaction(int userId)
    {
       await _reactionRepository.DeleteReaction(userId);
    }
}