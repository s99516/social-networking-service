﻿using SocialNetworkingServiceApp.IData.Comment;
using SocialNetworkingServiceApp.IData.User;
using SocialNetworkingServiceApp.IServices.Comment;
using SocialNetworkingServiceApp.IServices.Requests;

namespace SocialNetworkingSeviceApp.Service.Comment;

public class CommentService : ICommentService
{
    private readonly ICommentRepository _commentRepository;

    public CommentService(ICommentRepository commentRepository)
    {
        _commentRepository = commentRepository;
    }
    public Task<SocialNetworkingServiceApp.Domain.Comment.Comment> GetCommentById(int commentId)
    {
        return _commentRepository.GetComment(commentId);
    }

    public Task<IEnumerable<SocialNetworkingServiceApp.Domain.Comment.Comment>> GetCommentsByPostId(int postId)
    {
        return _commentRepository.GetCommentsByPostId(postId);
    }

    public Task<IEnumerable<SocialNetworkingServiceApp.Domain.Comment.Comment>> GetSubCommentsByParentCommentId(int parentCommentId)
    {
        return _commentRepository.GetCommentsByParentCommentId(parentCommentId);
    }

    public async Task<SocialNetworkingServiceApp.Domain.Comment.Comment> CreateComment(CreateComment createComment)
    {
        var comment = new SocialNetworkingServiceApp.Domain.Comment.Comment(createComment.ParentCommentId,
            createComment.PostId, createComment.UserId,
            createComment.UserNickName, createComment.CommentBody);
        comment.CommentId = await _commentRepository.AddComment(comment);
        return comment;
    }

    public async Task EditComment(EditComment editComment, int commentId)
    {
        var comment = await _commentRepository.GetComment(commentId);
        comment.EditComment(editComment.CommentBody);
        await _commentRepository.EditComment(comment);
    }

    public async Task DeleteComment(int commentId)
    {
        await _commentRepository.DeleteComment(commentId);
    }
}