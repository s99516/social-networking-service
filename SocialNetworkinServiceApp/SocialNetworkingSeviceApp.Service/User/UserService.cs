﻿using System.Threading.Tasks;
using SocialNetworkingServiceApp.IData.User;
using SocialNetworkingServiceApp.IData.UserRelation;
using SocialNetworkingServiceApp.IServices.Requests;
using SocialNetworkingServiceApp.IServices.User;

namespace SocialNetworkingSeviceApp.Service.User
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IUserRelationRepository _userRelationRepository;

        public UserService(IUserRepository userRepository, IUserRelationRepository relationRepository)
        {
            _userRepository = userRepository;
            _userRelationRepository = relationRepository;
        }

        public async Task<SocialNetworkingServiceApp.Domain.User.User> GetUserById(int userId)
        {
            return await _userRepository.GetUser(userId);
        }

        public async Task<SocialNetworkingServiceApp.Domain.User.User> GetUserByNickName(string nickName)
        {
            return await _userRepository.GetUser(nickName);
        }

        public async Task<SocialNetworkingServiceApp.Domain.User.User> CreateUser(CreateUser createUser)
        {
            var user = new SocialNetworkingServiceApp.Domain.User.User(createUser.NickName, createUser.FirstName,
                createUser.LastName, createUser.Gender, createUser.Email, createUser.Password, createUser.BirthDate);
            user.UserId = await _userRepository.AddUser(user);
            return user;
        }

        public async Task EditUser(EditUser editUser, int userId)
        {
            var user = await _userRepository.GetUser(userId);
            user.EditUser(editUser.NickName, editUser.FirstName, editUser.LastName, editUser.Gender, editUser.Email,
                editUser.BirthDate);
            
            await _userRepository.EditUser(user);
        }

        public async Task<IEnumerable<SocialNetworkingServiceApp.Domain.User.User>> GetRelatedUsersOfUserByUserId(int userId)
        {
            var usersIdList = await _userRelationRepository.GetRelatedUsersIdsOfUserByUserId(userId);
            var users = new List<SocialNetworkingServiceApp.Domain.User.User>();
            foreach (var id in usersIdList)
            {
                users.Add(await _userRepository.GetUser(id));
            }
            return users.ToList();
        }

        public async  Task<IEnumerable<SocialNetworkingServiceApp.Domain.User.User>> GetRelatingUsersOfUserByUserId(int userId)
        {
            var usersIdList = await _userRelationRepository.GetRelatingUsersIdsOfUserByUserId(userId);
            var users = new List<SocialNetworkingServiceApp.Domain.User.User>();
            foreach (var id in usersIdList)
            {
                users.Add(await _userRepository.GetUser(id));
            }
            return users.ToList();
        }

        public async Task DeleteUser(int userId)
        {
            await _userRepository.DeleteUser(userId);
        }

        public async Task AddUserToGroup(int userId, int groupid)
        {
            await _userRepository.AddUserToGroup(userId, groupid);
        }
    }
}